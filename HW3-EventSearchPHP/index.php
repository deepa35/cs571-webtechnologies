<?php
include 'geoHash.php';

$keyword = "";
$category = "";
$distance = 0;
$lat = NULL;
$long = NULL;
$locRadioSelection = "";
$hash = NULL;
$segmentID = NULL;
$eventdetails = "";

if ($_SERVER["REQUEST_METHOD"] == "POST" && (array_key_exists('eventID', $_POST) == TRUE || array_key_exists('eventVenue', $_POST) == TRUE)) {
    if (array_key_exists('eventID', $_POST) == TRUE) {
        $eventID = $_POST['eventID'];
        $detailReqUrl = "https://app.ticketmaster.com/discovery/v2/events/" . $eventID . ".json?apikey=Buwj0G2A846oGezBw3N05bgYt9YgUx5s";
        $detailsJSON = file_get_contents($detailReqUrl);
        sleep(1);
        exit($detailsJSON);
    } else {
        $eventVenue = $_POST['eventVenue'];
        $venueReqUrl = "https://app.ticketmaster.com/discovery/v2/venues.json?apikey=Buwj0G2A846oGezBw3N05bgYt9YgUx5s&keyword=" . urlencode($eventVenue);
        $venueJSON = file_get_contents($venueReqUrl);
        exit($venueJSON);
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && ! (array_key_exists('eventID', $_POST) == TRUE || array_key_exists('eventVenue', $_POST) == TRUE)) {
    $keyword = $_POST["keywordbox"];
    $category = $_POST["category_name"];
    $distance = $_POST["distancebox"];
    if ($distance == 0) {
        $distance = 10;
    }
    $locRadioSelection = $_POST["location"];
    if ($locRadioSelection == "here") {
        $location = json_decode($_POST["currentLocation"]);
        $lat = $location->lat;
        $long = $location->lon;
    } else {
        $locKeyWord = $_POST["locinputbox"];
        // Google maps get location
        $googleMapReqUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($locKeyWord) . "&key=AIzaSyDSvNSOBQhqi-J0jNCHQeYT60p9ES4OLig";
        $location = json_decode(file_get_contents($googleMapReqUrl));
        $lat = $location->results[0]->geometry->location->lat;
        $long = $location->results[0]->geometry->location->lng;
    }
    $hash = encode($lat, $long);
    if ($category == "default") {
        $segmentID = "";
    } else if ($category == "music") {
        $segmentID = "KZFzniwnSyZfZ7v7nJ";
    } else if ($category == "sports") {
        $segmentID = "KZFzniwnSyZfZ7v7nE";
    } else if ($category == "arts") {
        $segmentID = "KZFzniwnSyZfZ7v7na";
    } else if ($category == "film") {
        $segmentID = "KZFzniwnSyZfZ7v7nn";
    } else {
        $segmentID = "KZFzniwnSyZfZ7v7n1";
    }
    $ticketMasterUrl = "https://app.ticketmaster.com/discovery/v2/events.json?apikey=Buwj0G2A846oGezBw3N05bgYt9YgUx5s&keyword=" . urlencode($keyword) . "&segmentId=" . $segmentID . "&radius=" . $distance . "&unit=miles&geoPoint=" . $hash;
    $eventdetails = file_get_contents($ticketMasterUrl);
    $eventdetailsDecoded = json_decode($eventdetails, TRUE);
    array_push($eventdetailsDecoded, array(
        "sourceLat" => $lat,
        "sourceLong" => $long
    ));
    exit(json_encode($eventdetailsDecoded));
}

?>

<html>
<head>

<meta charset="UTF-8">
<title>EventSearch</title>
<style>
#searchboxcontainer {
	border: 3px solid #C8C8C8;
	height: 200px;
	width: 550px;
	margin: auto;
	margin-top: 100px;
	background-color: #FAFAFA;
}

#searchid {
	margin: 2%;
}

#submit_button:active, #clear_button:active {
    background-color: Transparent;
}

#linespace {
	line-height: 1.5;
}

#title {
	font-style: italic;
	font-size: 30px;
	text-align: center;
	margin: 0;
}

#lister ul {
	list-style: none;
	display: inline;
	position: absolute;
	margin: 0;
	padding: 0;
}

#noRecFound {
	border: 3px solid #C8C8C8;
	background-color: #FAFAFA;
	text-align: center;
	width: 1000px;
	margin: auto;
}

.eventsTable table, .eventsTable th, .eventsTable tr, .eventsTable td {
	border: 3px solid #D3D3D3;
	border-collapse: collapse;
	font-size: 18px;
	margin: 0px auto;
}

.eventsTable td {
	padding-left: 10px;
	padding-right: 10px;
	width: auto;
}

a.eventDisplayLink:link {
	text-decoration: none;
	color: black;
}

a.eventDisplayLink:visited {
	text-decoration: none;
	color: black;
}

a.eventDisplayLink:hover {
	text-decoration: none;
	color: grey;
}

.detailtable table, .detailtable th, .detailtable tr, .detailtable td {
	text-align: centre;
	font-size: 18px;
	margin: 0px auto;
}

.detailtable h2 {
	font-size: 18px;
	text-align: centre;
}

.venueHolder {
	text-align: center;
	margin: 0 auto;
	color: grey;
	font-size: 18px;
}

#firstSet, #secondSet {
	line-height: 2em;
}

.venueLocImgs {
	display: block;
	margin-left: auto;
	margin-right: auto;
	max-width: 80%;
}

.mapTable {
	margin: auto;
}

#map {
	height: 400px;
	width: 70%;
	margin: 20px 20px 20px 20px;
}

#mapHolder {
	height: 300px;
	width: 420px;
}
.dataset {
    line-height: 1em;

}

button {
    display: block;
    background-color: inherit;
    color: black;
    width: 100%;
    border: none;
    outline: none;
    text-align: center;
    cursor: pointer;
    font-family: "Times New Roman", Times, serif;
    font-size:16px;
    padding: 10px 10px 10px 10px;
    
}
button:hover {
	color: grey;
	background-color:#D3D3D3;
}

</style>

</head>
<body>
	<div id="searchboxcontainer">
		<form name="searchbox" id="searchid" method="post">
			<p id="title">Events Search</p>
			<hr color="#E8E8E8">
			<div id="linespace">
				<label for="keyword"><b>Keyword </b></label> <input type="text"
					name="keywordbox" id="keyword" required> <br> <label for="category"><b>Category</b></label>
				<select name="category_name" id="category">
					<option value="default">Default</option>
					<option value="music">Music</option>
					<option value="sports">Sports</option>
					<option value="arts">Arts & Theatre</option>
					<option value="film">Film</option>
					<option value="miscellaneous">Miscellaneous</option>
				</select> <br>
				<div id="lister">
					<label for="distance"><b>Distance (miles) </b></label> <input
						type="text" name="distancebox" id="distance" placeholder="10"> <b>from</b>&nbsp;&nbsp;
					<ul>
						<li><input type="radio" name="location" id="here" value="here"
							checked onchange="loc_enabledisabler()"><label for="here">Here</label>
						</li>
						<li><input type="radio" name="location" value="position"
							id="position" onchange="loc_enabledisabler()"> <label
							for="position"> <input type="text" placeholder="location"
								name="locinputbox" id="locinputbox" required disabled>
						</label></li>
					</ul>
					<input type="hidden" id="currentLocation" name="currentLocation">
				</div>
			</div>
			<br /> <input type="submit" name="submit_button" value="Search"
				id="submit_button" style="margin-left: 6em; margin-right: 1em;"
				disabled><input type="button" name="clear_button" value="Clear" id="clear_button"
				onclick="clear_form()">
		</form>
	</div>
	<div id="eventsData"></div>
	<script defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSvNSOBQhqi-J0jNCHQeYT60p9ES4OLig&callback=initMap"></script>
	<script type="text/javascript">

var eventJSON = "";
var detailsJSON = "";
var venueJSON = "";

//Finding the current location
var ipiApiUrl = "http://ip-api.com/json";
var arrowUp = "http://csci571.com/hw/hw6/images/arrow_up.png";
var arrowDown = "http://csci571.com/hw/hw6/images/arrow_down.png";
var latitudeSource = "";
var longitudeSource = "";
var latitudeDest = "";
var longitudeDest = "";
var map = null;
var marker = null;
var directionsService = null;
var directionsDisplay  = null;
var outerMapDestLat = "";
var outerMapDestLong = "";

var currentLocHTTP = new XMLHttpRequest();
currentLocHTTP.open("POST", ipiApiUrl, false);
currentLocHTTP.onreadystatechange = function() {
	if (this.readyState === 4 && this.status === 200) {
		document.getElementById("submit_button").removeAttribute("disabled");
		var response = currentLocHTTP.responseText;
		document.getElementById("currentLocation").value = response;
	}
}
currentLocHTTP.send();

function loc_enabledisabler() {
	var textBox = document.getElementById("locinputbox");
	var hereButton = document.getElementById("here");
	var locButton = document.getElementById("position");

	if(hereButton.checked) {
		textBox.value = "";
		textBox.setAttribute("disabled",true);
	} else {
		textBox.removeAttribute("disabled");
	}
}

function clear_form() {
	document.getElementById("keyword").value = "";
	document.getElementById("category").value = "default";
	document.getElementById("distance").value = "";
	document.getElementById("here").checked = true;
	document.getElementById("locinputbox").value = "";
	loc_enabledisabler();
	var results = document.getElementById("eventsData");
	while (results.firstChild) {
		results.removeChild(results.firstChild);
	}
}

document.getElementById("searchid").addEventListener("submit", function(event){
	event.preventDefault();
	var formDataArray = [];
	var formData = new FormData(this);
	for(each of formData) {
		formDataArray.push(encodeURIComponent(each[0]) + '=' + encodeURIComponent(each[1]));
	}
	var urlData =formDataArray.join('&').replace(/%20/g, '+');
	var xmlReq = new XMLHttpRequest();
	xmlReq.open('POST', '<?php echo $_SERVER["PHP_SELF"];?>', false);
	xmlReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xmlReq.send(urlData);
	var response = xmlReq.responseText;
	try { eventJSON = JSON.parse(response);	} catch (error) {}
	console.log(eventJSON);
	drawTable(eventJSON);
});


function drawTable(json) {
	var displayDiv = document.getElementById("eventsData");
	while (displayDiv.firstChild) {
		displayDiv.removeChild(displayDiv.firstChild);
	}
	var htmlText = "";
	if(!json._embedded || !json._embedded.events) {
		//display no records found
		htmlText+="<br><br><div id='noRecFound'>No Records have been found</div>";
		displayDiv.innerHTML = htmlText;
		return;
	}

	try { latitudeSource =json[0].sourceLat; } catch(error) {}
	try { longitudeSource = json[0].sourceLong; } catch(error) {}
	
	var events = json._embedded.events;
	htmlText += "<br><br><div class='eventsTable'><table style='width:70%;'><tr><th>Date</th><th>Icon</th><th>Event</th><th>Genre</th><th>Venue</th></tr>";
	for (var i=0; i< events.length;i++) {
		var eachEvent = events[i];
		var eveDate = "";
		try { eveDate = eachEvent.dates.start.localDate; } catch(e) {}
		var eveTime = "";
		try { eveTime = eachEvent.dates.start.localTime; } catch(e) {}
		var icon = "";
		try { icon = eachEvent.images[0].url; } catch(e) {}
		var event = "";
		try { event = eachEvent.name; } catch(e) {}
		var genre = "";
		try { genre = eachEvent.classifications[0].segment.name; } catch(e) {}
		var venue = "";
		try { venue = eachEvent._embedded.venues[0].name; } catch(e) {}
		var venLat = "";
		try { venLat = eachEvent._embedded.venues[0].location.latitude; } catch(e)  {}
		var venLong = "";
		try { venLong = eachEvent._embedded.venues[0].location.longitude; } catch(e)  {}
		var eventId = "";
		try { eventId = eachEvent.id; } catch(e) {}
		
		htmlText += "<tr>";

		if(!eveDate || (Object.keys(eveDate).length === 0 && eveDate.constructor === Object) || eveDate.length === 0 || eveDate == 'Undefined') {
			eveDate = "N/A";
			eveTime = "";
		}
		if(!eveTime || (Object.keys(eveTime).length === 0 && eveTime.constructor === Object) || eveTime.length === 0 || eveTime == 'Undefined') {
			eveTime = "";
		}
		htmlText+=("<td style='text-align:center;overflow: hidden;white-space: nowrap;'>" + eveDate+"<br>"+eveTime+"</td>");

		if(!icon || (Object.keys(icon).length === 0 && icon.constructor === Object) || icon.length === 0||  icon == 'Undefined') {
			htmlText+=("<td>N/A</td>");
		} else {
			htmlText+=("<td style='margin:auto;text-align:center;'><img style='margin:auto;' src='"+ icon +"' width='"+100+"' height='"+50+"'></td>");
		}

		if(!event || (Object.keys(event).length === 0 && event.constructor === Object) || event.length === 0||  event == 'Undefined') {
			htmlText+=("<td>N/A</td>");
		} else {
			htmlText+=("<td><a class='eventDisplayLink' href=# onclick='displayDetails(\""+eventId +"\", \""+i+"\");event.preventDefault();'> "+ event+"</a></td>");
		}

		if(!genre || (Object.keys(genre).length === 0 && genre.constructor === Object) || genre.length === 0||  genre == 'Undefined') {
			htmlText+=("<td>N/A</td>");
		} else {
			htmlText+=("<td>" + genre +"</td>");
		}
		
		if(!venue || (Object.keys(venue).length === 0 && venue.constructor === Object) || venue.length === 0|| venue == 'Undefined') {
			htmlText+=("<td>N/A</td>");
		} else {
			if(venLat && venLong && venLat != 'Undefined' && venLong != 'Undefined') {
				htmlText+=("<td id='venue"+i+"'><a class='eventDisplayLink' href=# onclick='showMap(\""+ venLat +"\",\""+ venLong +"\",\"venue"+i+"\");event.preventDefault();'>" + venue +"</a></td>");
			} else {
				htmlText+=("<td id='venue"+i+"'>" + venue +"</td>");
			}
		}
		
		htmlText+="</tr>";
		
	}
	htmlText += "</table><br><br></div>";
	htmlText += "<div id='mapHolder' currentID = '' style='display:none;position: absolute; top: 0px; left: 0px;'></div>";
	htmlText += "<div id = 'buttonHolder' style='display:none;text-align: center; background-color: #F0F0F0; position: absolute; top: 0px; left: 0px; height: auto; width: 95px;'>";
	htmlText += "<button class='travelButton' onclick='markDirectionsOuter(\"WALKING\");'>Walk there</button>";
	htmlText += "<button class='travelButton' onclick='markDirectionsOuter(\"BICYCLING\");'>Bike there</button>";
	htmlText += "<button class='travelButton' onclick='markDirectionsOuter(\"DRIVING\");'>Drive there</button></div>";
	displayDiv.innerHTML = htmlText;
	return;
}

function displayDetails(id,index) {
	var venue = "";
	try { venue = eventJSON._embedded.events[index]._embedded.venues[0].name; } catch(e) {}
	var xmlReq = new XMLHttpRequest();
	xmlReq.open('POST', '<?php echo $_SERVER["PHP_SELF"];?>', false);
	xmlReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	var urlData = encodeURIComponent("eventID") + '=' + encodeURIComponent(id);
	urlData = urlData.replace(/%20/g, '+');
	xmlReq.send(urlData);
	var response = xmlReq.responseText;
		
	try { detailsJSON = JSON.parse(response);	} catch (error) {}
	console.log(detailsJSON);

	xmlReq = new XMLHttpRequest();
	xmlReq.open('POST', '<?php echo $_SERVER["PHP_SELF"];?>', false);
	xmlReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	urlData = encodeURIComponent("eventVenue") + '=' + encodeURIComponent(venue);
	urlData = urlData.replace(/%20/g, '+');
	xmlReq.send(urlData);
	response = xmlReq.responseText;
		
	try { venueJSON = JSON.parse(response);		} catch (error) {} 
	console.log(venueJSON);
	displayEvent();
}

function displayEvent() {
	var displayDiv = document.getElementById("eventsData");
	while (displayDiv.firstChild) {
		displayDiv.removeChild(displayDiv.firstChild);
	}

	var heading = detailsJSON.name;

	//Date section
	var finalDate = "";
	var date = "";
	try { date = detailsJSON.dates.start.localDate; } catch(error) {}
	var time = "";
	try { time = detailsJSON.dates.start.localTime; } catch(error) {}

	if( date && time ) {
		finalDate = date + " " + time;
	} else if (date && !time) {
		finalDate = date;
	}

	//Artists section
	var artistElem = null;
	var artists = "";
	try { artistElem = detailsJSON._embedded.attractions; } catch(error) {}

	if(artistElem && artistElem.length>0) {
		var artistUrl = "";
		var artistName = "";
		try { artistUrl = artistElem[0].url; } catch(error) {}
		try { artistName = artistElem[0].name; } catch(error) {}
		if(artistName && artistName != 'Undefined') {
    		if(	artistUrl && artistUrl != 'Undefined') {
    			artists += "<a class='eventDisplayLink' target='_blank' href='"+artistUrl+"'>" + artistName + "</a>";
    		} else {
    			artists += artistName;
    		}
		}
		for (var i=1; i< artistElem.length;i++) {
			try { artistUrl = artistElem[i].url; } catch(error) {}
			try { artistName = artistElem[i].name; } catch(error) {}
			if(artistName && artistName != 'Undefined') {
	    		if(	artistUrl && artistUrl != 'Undefined') {
		    		if(artists.length == 0) {
		    			artists += "<a class='eventDisplayLink' target='_blank' href='"+artistUrl+"'>" + artistName + "</a>";
		    		} else {
	    				artists += " | " + "<a class='eventDisplayLink' target='_blank' href='"+artistUrl+"'>" + artistName + "</a>";
		    		}
	    		} else {
	    			if(artists.length == 0) {
	    				artists += artistName;
	    			} else {
	    				artists += " | " + artistName;
	    			}
	    		}
			}
		}
	}

	//Venue section
	var venue = "";
	try { venue = detailsJSON._embedded.venues[0].name; } catch(error) {}

	//Genre section
	var finalGenre = "";
	var genre = "";
	try { genre = detailsJSON.classifications[0].genre.name; } catch(error) {}
	var subGenre = "";
	try { subGenre = detailsJSON.classifications[0].subGenre.name; } catch(error) {}
	var segment = "";
	try { segment = detailsJSON.classifications[0].segment.name; } catch(error) {}
	var subType = "";
	try { subType = detailsJSON.classifications[0].subType.name; } catch(error) {}
	var type = "";
	try { type = detailsJSON.classifications[0].type.name; } catch(error) {}

	if(subGenre && subGenre != 'Undefined') {
		finalGenre += subGenre + " | ";
	}
	if(genre &&  genre != 'Undefined') {
		finalGenre += genre + " | ";
	}
	if(segment && segment != 'Undefined') {
		finalGenre += segment + " | ";
	}
	if (subType && subType != 'Undefined') {
		finalGenre += subType + " | ";
	}
	if(type && type != 'Undefined') {
		finalGenre += type + " | ";
	}
	
	if(finalGenre && finalGenre.length!=0 && finalGenre != 'Undefined') {
		finalGenre = finalGenre.slice(0,-3);
	}
	
	//Price range section
	var minPrice = "";
	var priceRange = "";
	try { minPrice = detailsJSON.priceRanges[0].min; } catch(error) {}
	var maxPrice = "";
	try { maxPrice = detailsJSON.priceRanges[0].max; } catch(error) {}
	if((maxPrice.length !=0 && maxPrice != 'Undefined') && (minPrice.length == 0 || minPrice == 'Undefined' )) {
		priceRange = maxPrice;
	} else if ((maxPrice.length == 0 || maxPrice == 'Undefined') && (minPrice.length != 0 && minPrice != 'Undefined')) {
		priceRange = minPrice;
	} else if ((maxPrice.length != 0  && maxPrice != 'Undefined')&& (minPrice.length != 0 && minPrice != 'Undefined')) {
		priceRange = minPrice + " - " + maxPrice;
	}
	

	var currency = "";
	try { currency = detailsJSON.priceRanges[0].currency; } catch(error) {}
	if(currency == 'Undefined') {
		currency = "";
	}

	//Ticket Status section
	var status = "";
	try { status = detailsJSON.dates.status.code; } catch(error) {}
	if (status && status != 'Undefined') {
		status = status.charAt(0).toUpperCase() + status.slice(1);
	}

	//Buytickets section
	var buyTicket = "";
	try { buyTicket = detailsJSON.url; } catch(error) {}

	//SeatMap section
	var seatMapUrl = "";
	try { seatMapUrl = detailsJSON.seatmap.staticUrl; } catch(error) {}

	var htmlTxt = "<div class='detailtable'><br><br><h2 align='center' style='font-size:24px;'>"+heading+"</h2>";
	if(seatMapUrl.length == 0 ||  seatMapUrl == 'Undefined') {
		htmlTxt += "<table style='width:auto;'><tr><td>";
	} else {
		htmlTxt += "<table style='width:70%;'><tr><td>";
	}
	if(finalDate.length != 0 &&  finalDate != 'Undefined') {
		htmlTxt += "<span class='dataset'><h2 align='left' style='font-size:22px;'>Date</h2>"+finalDate+"</span><br>";
	}
	if(artists.length!=0 &&  artists != 'Undefined') {
		htmlTxt += "<span class='dataset'><h2 align='left' style='font-size:22px;'>Artist / Team</h2>"+artists+"</span><br>";
	}
	if(venue.length != 0 &&  venue != 'Undefined') {
		htmlTxt += "<span class='dataset'><h2 align='left' style='font-size:22px;'>Venue</h2>"+venue+"</span><br>";
	}
	if(finalGenre.length != 0 &&  finalGenre != 'Undefined') {
		htmlTxt += "<span class='dataset'><h2 align='left' style='font-size:22px;'>Genres</h2>"+finalGenre+"</span><br>";
	}
	if(priceRange.length != 0 &&  priceRange != 'Undefined') {
		htmlTxt += "<span class='dataset'><h2 align='left' style='font-size:22px;'>Price Ranges</h2>"+priceRange+ " " +currency+"</span><br>";
	}
	if(status.length!=0 &&  status != 'Undefined') {
		htmlTxt += "<span class='dataset'><h2 align='left' style='font-size:22px;'>Ticket Status</h2>"+status+"</span><br>";
	}
	if(buyTicket.length!=0 &&  buyTicket != 'Undefined') {
		htmlTxt += "<span class='dataset'><h2 align='left' style='font-size:22px;'>Buy Ticket At:</h2><a class='eventDisplayLink' target='_blank' href='"+buyTicket+"'>Ticketmaster</a></span><br>";
	}

	htmlTxt+="</td>";

	if(seatMapUrl.length == 0||  seatMapUrl == 'Undefined') {
		htmlTxt+="</tr>";
	} else {
		htmlTxt+=("<td><img src='"+ seatMapUrl +"' width='"+700+"' height='"+400+"'></td>");
	}

	htmlTxt+="</table></div><br> <br>";
	displayDiv.innerHTML = htmlTxt;
	displayVenue();
	return;
}

function displayVenue() {
	var displayDiv = document.getElementById("eventsData");
	
	var venueDiv = document.createElement("div");
	venueDiv.setAttribute("class","venueHolder");
	venueDiv.style.margin = "0px auto";
	var tableTxt = "";
	var imageTableTxt = "";

	if(!venueJSON || !venueJSON._embedded || !venueJSON._embedded.venues) {
		//display no records found
		tableTxt += "<br><br><div id='noRecFound'>No Venue Info Found</div>";
	} else {
	
    	// Name field
    	var name = "N/A";
    	try { name = venueJSON._embedded.venues[0].name; } catch(error) {}
    	if(name == "Undefined") {
			name = "N/A";
    	}
    	
    	// Location
    	try { latitudeDest = venueJSON._embedded.venues[0].location.latitude; } catch(error) {}
    	try { longitudeDest = venueJSON._embedded.venues[0].location.longitude; } catch(error) {}
    	var mapShow = "N/A";
		if(latitudeDest && latitudeDest != "Undefined" && longitudeDest && longitudeDest != "Undefined" ) {
    		mapShow = getMapTable();
		}
    
    	//Address section
    	var address = "N/A";
    	try { address = venueJSON._embedded.venues[0].address.line1; } catch(error) {}
    	if(!address || address == "Undefined") {
    		address = "N/A";
    	}
    
    	//City section
    	var cityName = "N/A";
    	var stateCode = "N/A";
    	var city = "N/A"

    	try { cityName = venueJSON._embedded.venues[0].city.name; } catch(error) {}
    	if(!cityName || cityName == "Undefined") {
    		cityName = "N/A";
    	}
    	if(cityName != "N/A") {
    		try { stateCode = venueJSON._embedded.venues[0].state.stateCode; } catch(error) {}
    		if(!stateCode || stateCode == "Undefined" || stateCode == "N/A") {
    			city = cityName;
        	} else {
    			city = cityName + ", " + stateCode;	
    		}
    	}
    
    	//Postal code
    	var postalcode = "N/A";
    	try { postalcode = venueJSON._embedded.venues[0].postalCode; } catch(error) {}
    	if(!postalcode || postalcode == "Undefined") {
    		postalcode = "N/A";
    	}
    
    	//Upcoming events
    	var upcomingEventURL = "N/A";
    	var upcomingEvents = "N/A";
    	try { upcomingEventURL = venueJSON._embedded.venues[0].url; } catch(error) {}
    	if(!upcomingEventURL || upcomingEventURL == "Undefined") {
    		upcomingEventURL = "N/A";
    	}
    	if(upcomingEventURL != "N/A") {
    		upcomingEvents = "<a class='eventDisplayLink' target='_blank' href='"+ upcomingEventURL +"'>" + name + " Tickets</a>";
    	}
    
    	tableTxt = "<br><br><div class='eventsTable'><table style='width:70%;'>";
    	tableTxt += "<tr><td style='text-align:right;'><b>Name</b></td><td style='text-align:center;'>"+name+"</td></tr>";
    	if(mapShow == "N/A") {
    		tableTxt += "<tr><td style='text-align:right;'><b>Map</b></td><td style='text-align:center;'>"+mapShow+"</td></tr>";
    	} else {
    		tableTxt += "<tr><td style='text-align:right;'><b>Map</b></td><td>"+mapShow+"</td></tr>";
    	}
    	tableTxt += "<tr><td style='text-align:right;'><b>Address</b></td><td style='text-align:center;'>"+address+"</td></tr>";
    	tableTxt += "<tr><td style='text-align:right;'><b>City</b></td><td style='text-align:center;'>"+city+"</td></tr>";
    	tableTxt += "<tr><td style='text-align:right;'><b>Postal Code</b></td><td style='text-align:center;'>"+postalcode+"</td></tr>";
    	tableTxt += "<tr><td style='text-align:right;'><b>Upcoming Events</b></td><td style='text-align:center;'>"+upcomingEvents+"</td></tr>";
    	tableTxt += "</table></div>";
	}

	if(!venueJSON || !venueJSON._embedded || !venueJSON._embedded.venues ||!venueJSON._embedded.venues[0].images ) {
		imageTableTxt += "<br><br><div id='noRecFound'>No Venue Photos Found</div>";
	} else {
		//Images section
    	var imagesSection = null;
    	var images = [];
    	try { imagesSection = venueJSON._embedded.venues[0].images; } catch(error) {}
    	if(imagesSection!=null) {
    		for(var i=0; i< imagesSection.length; i++) {
    			var image = null;
    			try { image = imagesSection[i].url } catch(error) {};
    			images.push(image);
    		}
		}
		
    	imageTableTxt = "<br><br><div class='eventsTable'><table style='width:70%;'>";
    	for(var i = 0; i < images.length; i++) {
    		imageTableTxt += "<tr><td style='text-align:center;'><img class='venueLocImgs' src='" + images[i]  + "' height=auto></td></tr>";
    	}
    	imageTableTxt+="</table></div>";
	}

	var htmlText = "<br>";
	htmlText += "<div><div id='firstSet' onclick='toggleImage(\"firstSet\");'><span>click to show venue info</span><br><img src='"+arrowDown+"' width='"+100+"' height='"+50+"' ></div><div style='display:none;' id='firstTable'>"+tableTxt+"</div></div>";
	htmlText += "<div><div id='secondSet' onclick='toggleImage(\"secondSet\");'><br><span>click to show venue photos</span><br><img src='"+arrowDown+"'width='"+100+"' height='"+50+"' ></div><div style='display:none;' id='secondTable'>"+imageTableTxt+"</div></div><br><br><br>";

	venueDiv.innerHTML = htmlText;
	displayDiv.appendChild(venueDiv);
	
	initMap();
}

function toggleImage(id) {
	var toHideElem = null;
	var tableElem = null;
	var tableElemToHide = null;
	if(id == "firstSet") {
		toHideElem = document.getElementById("secondSet");
		tableElem = document.getElementById("firstTable");
		tableElemToHide = document.getElementById("secondTable");
	} else {
		toHideElem = document.getElementById("firstSet");
		tableElem = document.getElementById("secondTable");
		tableElemToHide = document.getElementById("firstTable");
	}
	var elem = document.getElementById(id);
	var image = elem.getElementsByTagName('img')[0];
	var span = elem.getElementsByTagName('span')[0];
	if(image.src == "http://csci571.com/hw/hw6/images/arrow_down.png") {
		initMap();
		image.src = "http://csci571.com/hw/hw6/images/arrow_up.png";
		tableElem.style.display = "block";
		if(id == "firstSet") {
			span.innerHTML = "click to hide venue info";
		} else {
			span.innerHTML = "click to hide venue photos";
		}
	} else {
		image.src = "http://csci571.com/hw/hw6/images/arrow_down.png";
		tableElem.style.display = "none";
		if(id == "firstSet") {
			span.innerHTML = "click to show venue info";
		} else {
			span.innerHTML = "click to show venue photos";
		}
	}
	
	//handling Other Element
	var imageHid = toHideElem.getElementsByTagName('img')[0];
	var spanHid = toHideElem.getElementsByTagName('span')[0];
	imageHid.src = "http://csci571.com/hw/hw6/images/arrow_down.png";
	tableElemToHide.style.display = "none";
	if(id == "firstSet") {
		spanHid.innerHTML = "click to show venue photos";
	} else {
		spanHid.innerHTML = "click to show venue info";
	}
}

function getMapTable() {
	var mapTxt = "<div class ='maptable'>";
	mapTxt += "<div style='text-align:center;float: left; margin-top: 50px; margin-right: 20px;background-color:#F0F0F0;'>";
	mapTxt += "<a class='eventDisplayLink' href=# onclick='markDirections(\"WALKING\");event.preventDefault();'>Walk there</a><br>";
	mapTxt += "<a class='eventDisplayLink' href=# onclick='markDirections(\"BICYCLING\");event.preventDefault();'>Bike there</a><br>";
	mapTxt += "<a class='eventDisplayLink' href=# onclick='markDirections(\"DRIVING\");event.preventDefault();'>Drive there</a></div>";
	mapTxt += "<div id='map'></div>";
	mapTxt += "</div>";
	return mapTxt;
}

function initMap() {
	var dest = { lat: parseFloat(latitudeDest), lng:parseFloat(longitudeDest) };
	try {
		map = new google.maps.Map(document.getElementById('map'), {zoom: 13, center: dest});  
		marker = new google.maps.Marker({position: dest, map: map});
		directionsService = new google.maps.DirectionsService();
		directionsDisplay = new google.maps.DirectionsRenderer();
	} catch (error) {}
}

function markDirections(travel) {
	marker.setMap(null);
	directionsDisplay.setMap(map);
	var dest = { lat: parseFloat(latitudeDest), lng:parseFloat(longitudeDest) } ;
	var source = { lat: parseFloat(latitudeSource), lng:parseFloat(longitudeSource) };
	var request = {
		origin: source,
		destination: dest,
		travelMode: travel
	};
	directionsService.route(request, function(result, status) {
	if (status == 'OK') {
	    directionsDisplay.setDirections(result);
	}
	});
}

function showMap(venueLat, venueLong, id) {
	var linkElem = document.getElementById(id);
	var mapDest = document.getElementById("mapHolder");
	var buttonHolder = document.getElementById("buttonHolder");
	var currentID = mapDest.currentID;

	if (currentID!= id) {
		mapDest.currentID = id;
		outerMapDestLat = venueLat;
		outerMapDestLong = venueLong;
		var bound = linkElem.getBoundingClientRect();
		
		var dest = {
			lat : parseFloat(venueLat),
			lng : parseFloat(venueLong)
		};
		try {
			map = new google.maps.Map(mapDest, {
				zoom : 13,
				center : dest
			});
			marker = new google.maps.Marker({
				position : dest,
				map : map
			});
			directionsService = new google.maps.DirectionsService();
			directionsDisplay = new google.maps.DirectionsRenderer();
		} catch (error) {
		}
		var offset = bound.top + window.pageYOffset;
		mapHolder.style.top = offset + 50 + "px";
		buttonHolder.style.top = offset + 50 + "px";
		mapHolder.style.left = bound.left + 5 + "px";
		buttonHolder.style.left = bound.left + 5 + "px";
		mapDest.style.display = "block"
		buttonHolder.style.display = "block";
	} else {
		mapDest.currentID = "";
		mapDest.style.display = "none";
		buttonHolder.style.display = "none";
	}
}
outerMapDestLat

function markDirectionsOuter(travel) {
	marker.setMap(null);
	directionsDisplay.setMap(map);
	var dest = { lat: parseFloat(outerMapDestLat), lng:parseFloat(outerMapDestLong) } ;
	var source = { lat: parseFloat(latitudeSource), lng:parseFloat(longitudeSource) };
	var request = {
		origin: source,
		destination: dest,
		travelMode: travel
	};
	directionsService.route(request, function(result, status) {
	if (status == 'OK') {
	    directionsDisplay.setDirections(result);
	}
	});
}

</script>

</body>
</html>

