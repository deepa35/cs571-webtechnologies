var travelEntSearchApp = angular.module('ESApp', ['ngMaterial','ngAnimate','angular-svg-round-progressbar']);
var currentLocJSON = "";
var autoCompleteJSON="";
var eventSearchJSON = "";
var eventDetailsJSON = "";
var artistsJSON = [];
var venueDetailsJSON = "";
var upcomingEventsJSON = "";
var map = null
var marker = null;
var warnedElem = null;
var googleSearchSuccess = false;
var spotifySearchSuccess = false;
var resultsFailed = false;


travelEntSearchApp.controller('entSearchAppController', function entSearchAppController($scope,$http,$timeout, $q) {
    $scope.disableOnLoc = true;
    $scope.artists = "";
    $scope.tableMode = 'results';
    $scope.currentTableModeForEvent = "";
    $scope.parseFloat = parseFloat;
    $scope.category = "";
    $scope.user = {locRadio:"here",category_name:"default", distance_unit:"miles"};
    $scope.lasttab = {upcomingOptions:'default',sortOrder:'ascend'};
    $scope.show_cards = false;
    var originalForm = angular.copy($scope.user);
    var self = this;
    self.querySearch = querySearch;
    $scope.searchOn = false;

    $(document).ready(function() {
        $.ajax({
            url: 'http://ip-api.com/json',
            type: 'GET'
        }).done(function(data) {
            currentLocJSON = data;
            $scope.$apply(function() {
                $scope.disableOnLoc = false;
            });
            console.log(currentLocJSON);
        });
    });

    $scope.resetForm = function () {
        $('#results_tab').click();
        $("#firstSection").addClass('no-animate');
        $scope.enableProgress = false;
        $scope.enableDetailsButton = false;
        $scope.enableNoResults = false;
        $scope.enableFailedResults = false;
        $scope.enableResultTable = false;
        $scope.enableDetailsSection = false;
        $scope.tableMode = 'results';
        eventSearchJSON = "";
        eventDetailsJSON = "";
        venueDetailsJSON = ""
        artistsJSON = "";
        upcomingEventsJSON = "";
        $scope.venueInfo = "";
        $scope.eachEventInfo = "";
        $scope.upcomingInfo = "";
        $scope.artistsInfo = "";
        $scope.artists = "";
        $scope.currentTableModeForEvent = "";
        $scope.category = "";
        $scope.resetFailedAndNoResults();
        googleSearchSuccess = false;
        spotifySearchSuccess = false;
        $scope.show_cards = false;
        $scope.selectedEvent = ""
        $scope.searchOn = false;
        resultsFailed = false;

        self.searchText = "";
        $scope.user = angular.copy(originalForm);

        $("#location").prop("disabled", true);
        $scope.myForm.$setPristine();
        $scope.myForm.location_name.$setUntouched();
        $scope.myForm.keyword_name.$setUntouched();
    }

    $('[name="locRadio"]').click(function() {
        if($("input[name=locRadio]:checked").val() == "others") {
            $("#location").removeAttr("disabled");

        } else if($("input[name=locRadio]:checked").val() == "here") {
            $scope.myForm.location_name.$setPristine();
            $scope.myForm.location_name.$setUntouched();
            $scope.myForm.location_name.$setPristine();
            $("#location").prop("disabled", true);
            $scope.user.location_name = null;
        }
    });

    function querySearch (query) {
        let info = {"keyword":query};
        return $http({
            method: 'GET',
            url : '/autoComplete',
            params: info
        }).then(function(response){
                try {autoCompleteJSON = response.data._embedded.attractions;} catch(error) {}

                    if (autoCompleteJSON != "" || autoCompleteJSON != undefined) {
                        return autoCompleteJSON;
                    }
                    return [];
                },
                function (data) {
                    return [];
                });

    }

    $scope.submit = function(userData) {
        $("#firstSection").addClass('no-animate');
        $scope.enableDetailsSection = false;
        $scope.enableResultTable = false;
        $scope.enableNoResults = false;
        $scope.enableFailedResults = false;
        $scope.enableDetailsButton = false;
        $scope.enableProgress = true;
        $scope.tableMode = 'results';
        $('#results_tab').click();
        eventSearchJSON = "";
        eventDetailsJSON = "";
        venueDetailsJSON = ""
        artistsJSON = "";
        upcomingEventsJSON = "";
        $scope.eachEventInfo = "";
        $scope.venueInfo = "";
        $scope.upcomingInfo = "";
        $scope.artistsInfo = "";
        $scope.artists = "";
        $scope.currentTableModeForEvent = "";
        $scope.category = ""
        $scope.resetFailedAndNoResults()
        googleSearchSuccess = false;
        spotifySearchSuccess = false;
        $scope.show_cards = false;
        $scope.selectedEvent = "";
        $scope.searchOn = true;
        resultsFailed = false;

        let keyword = $scope.myForm.keyword_name.$viewValue;
        let category = userData.category_name;
        let distanceUnit = userData.distance_unit;
        let radioButton = userData.locRadio;
        let distance = userData.distance_value;
        let location = userData.location_name;

        let formData = {"keyword": keyword,
            "category": category,
            "distance_unit":distanceUnit,
            "radioButton": radioButton,
            "distance":distance,
            "locationInput":location,
            "currentLocation": currentLocJSON
        };

        $http({
            method: 'GET',
            url: '/eventSearch',
            params: formData
        }).then(function(response) {
                eventSearchJSON = response.data;
                console.log(eventSearchJSON);
                $scope.showEventListTable('results');
            },
            function(data) {
                $scope.enableProgress = false;
                $scope.enableDetailsButton = false;
                $scope.enableNoResults = false;
                $scope.enableFailedResults = true;
                $scope.enableResultTable = false;
                $scope.enableDetailsSection = false;
                //$scope.enableFirstPage = true;
                resultsFailed = true;

                console.log(data);
            });
    }

    $scope.initMap = function() {
        try {
            var latitude = venueDetailsJSON._embedded.venues[0].location.latitude;
            var longitude = venueDetailsJSON._embedded.venues[0].location.longitude;
            var dest = {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            };
            map = new google.maps.Map(document.getElementById('mapHolder'), {zoom: 13, center: dest});
            marker = new google.maps.Marker({position: dest, map: map});

        } catch(error) { console.log(error)}
    }

    $scope.showEventListTable = function(mode) {
        var tableList = null;
        //$scope.enableSecondPage = false;

        if(mode == 'results') {
            $scope.tableMode = 'results';
            try {tableList = eventSearchJSON._embedded.events; } catch(error) {}
        } else {
            $scope.tableMode = 'favs';
            tableList = [];
            for(var i=0;i<localStorage.length;i++) {
                let key = i+10;
                tableList.push(JSON.parse(localStorage.getItem(key)));
            }
        }
        if(tableList == null || tableList == undefined || !tableList || tableList.length == 0) {
            $scope.enableProgress = false;
            $scope.enableDetailsButton = false;
            if(mode == 'favs') {
                $scope.enableNoResults = true;
                $scope.enableFailedResults = false;
            } else {
                if($scope.searchOn) {
                    if(resultsFailed == false) {
                        $scope.enableNoResults = true;
                        $scope.enableFailedResults = false;
                    } else {
                        $scope.enableNoResults = false;
                        $scope.enableFailedResults = true;
                    }
                } else {
                    $scope.enableNoResults = false;
                    $scope.enableFailedResults = false;
                }
            }

            $scope.enableResultTable = false;
            $scope.enableDetailsSection = false;
            //$scope.enableFirstPage = true;
        } else {
            if(mode == 'results') {
                var sorted_result = tableList.sort(function (a, b) {
                    let date1 = new Date(a.dates.start.localDate);
                    let date2 = new Date(b.dates.start.localDate);

                    return date1 - date2;
                });
                $scope.eventsList = sorted_result;
            } else {
                $scope.eventsList = tableList;
            }
            if($scope.currentTableModeForEvent == $scope.tableMode) {
                $scope.disableDetails = false;
                if($scope.tableMode == 'favs' && !$scope.isCurrentEventInFavorites(eventDetailsJSON)) {
                    $scope.disableDetails = true;
                }
            } else {
                $scope.disableDetails = true;
            }
            $scope.enableProgress = false;
            $scope.enableDetailsButton = true;
            $scope.enableNoResults = false;
            $scope.enableFailedResults = false;
            $scope.enableResultTable = true;
            $scope.enableDetailsSection = false;
            //$scope.enableFirstPage = true;
        }
    }

    $scope.isCurrentEventInFavorites = function(eventJSON) {
        if($scope.getRealItemForID(eventJSON.id) == -1) {
            return false;
        }
        return true;
    }

    $scope.addToFavorites = function (eventJSON) {
        if($scope.getRealItemForID(eventJSON.id) == -1) {
            var index = localStorage.length+10;
            localStorage.setItem(index, JSON.stringify(eventJSON));
        } else {
            $scope.removeFromFavorites(eventJSON);
        }
    }

    $scope.addToFavoritesInEDPage = function (eventJSON) {
        var realID = $scope.getRealItemForID(eventJSON.id);
        if(realID == -1) {
            var index = localStorage.length+10;
            localStorage.setItem(index, JSON.stringify(eventJSON));
        } else {
            //localStorage.removeItem(realID);
            let index = realID;
            let forLoopIndex = index-10;
            var i;
            for(i= forLoopIndex; i<localStorage.length-1;i++) {
                localStorage[i+10] = localStorage[i+1+10]
            }
            localStorage.removeItem(i+10);
            /*if($scope.tableMode == 'favs') {
               $scope.disableDetails = true;
               //$scope.currentTableModeForEvent = "";
            }*/
        }
    }

    $scope.getRealItemForID = function (key) {
        for(var i=0; i<localStorage.length; i++) {
            let index = i+10;
            var value = localStorage.getItem(index)
            var valueJSON = "";
            try {
                valueJSON = JSON.parse(value);

                if (valueJSON.id == key) {
                    return index;
                }
            } catch(error) {}
        }
        return -1;
    }

    $scope.removeFromFavorites = function (eventJSON) {
        let index = $scope.getRealItemForID(eventJSON.id);
        let forLoopIndex = index-10;
        var i;
        for(i= forLoopIndex; i<localStorage.length-1;i++) {
            localStorage[i+10] = localStorage[i+1+10]
        }
        localStorage.removeItem(i+10);
        /* if(eventJSON.id == eventDetailsJSON.id && $scope.tableMode != 'favs' ) {
             //$scope.currentTableModeForEvent = "";
         }*/
        if($scope.tableMode == 'favs') {
            //$scope.disableDetails = true;
            //$scope.currentTableModeForEvent = "";
            $scope.showEventListTable('favs');
        }
    }

    $scope.isInLocalStorage = function (key) {
        if($scope.getRealItemForID(key) == -1) {
            return false;
        }
        return true;
    }

    $scope.showEventDetails = function (event, eventID, mode) {
        //$scope.enableFirstPage = false;
        $scope.selectedEvent = event;

        $scope.enableResultTable = false;
        let currentEventID = "";
        try {currentEventID = eventDetailsJSON.id} catch(error) {}
        $("#firstSection").removeClass('no-animate');

        //Events Tab
        if(currentEventID != eventID) {
            $scope.prograssBarEvent = true;
            $scope.prograssBarVenue = true;
            $scope.prograssBarUpcoming = true;
            $scope.prograssBarArtists = true;
            $scope.resetFailedAndNoResults();
            let idJSON = {
                "eventID": eventID
            }
            eventDetailsJSON = "";
            $scope.eachEventInfo = "";
            venueDetailsJSON = "";
            $scope.venueInfo = "";
            artistsJSON = "";
            googleSearchSuccess = false;
            spotifySearchSuccess = false;
            $scope.artistsInfo="";
            upcomingEventsJSON = "";
            $scope.upcomingInfo = "";
            $scope.show_cards = false;
            //$('#event-tab').click();

            $http({
                method: 'GET',
                url: '/fetchEventDetails',
                params: idJSON
            }).then(function (response) {
                    eventDetailsJSON = response.data;
                    $scope.selectedID = eventID;
                    console.log(eventDetailsJSON);
                    $scope.showEventDetailsPage(mode);
                    //Venue Info
                    $scope.getVenueInfo()
                },
                function (data) {
                    $scope.selectedID = eventID;
                    eventDetailsJSON = "";
                    $scope.eachEventInfo = "";
                    venueDetailsJSON = "";
                    $scope.venueInfo = "";
                    artistsJSON = "";
                    googleSearchSuccess = false;
                    spotifySearchSuccess = false;
                    $scope.artistsInfo="";
                    upcomingEventsJSON = "";
                    $scope.upcomingInfo = "";
                    $scope.currentTableModeForEvent = mode;
                    $scope.prograssBarEvent = false;
                    $scope.enableProgress = false;
                    $scope.enableDetailsButton = false;
                    $scope.enableNoResults = false;
                    $scope.enableNoResultsForEvents = false;
                    $scope.enableFailedResults = false;
                    $scope.enableFailedResultsForEvents = true;
                    $scope.enableFailedResultsForVenue = true;
                    $scope.enableFailedResultsForArtists = true;
                    $scope.enableFailedResultsForUpcoming = true;
                    $scope.enableResultTable = false;
                    $scope.enableDetailsSection = true;
                    $scope.prograssBarVenue = false;
                    $scope.prograssBarUpcoming = false;
                    $scope.prograssBarArtists = false;
                    $scope.enableResultTable = false;
                    //$scope.enableSecondPage = true;
                    console.log(data);
                });
        } else {
            $scope.showEventDetailsPage(mode);
        }
    }

    $scope.showEventDetailsPage = function (mode) {
        $scope.eachEventInfo = eventDetailsJSON;
        $scope.currentTableModeForEvent = mode;
        if( $scope.enableFailedResultsForEvents) {
            $scope.prograssBarEvent = false;
            $scope.currentTableModeForEvent = mode;
            $scope.enableProgress = false;
            $scope.enableDetailsButton = false;
            $scope.enableNoResults = false;
            $scope.enableNoResultsForEvents = false;
            $scope.enableFailedResults = false;
            $scope.enableFailedResultsForVenue = true;
            $scope.enableFailedResultsForArtists = true;
            $scope.enableFailedResultsForUpcoming = true;
            $scope.enableResultTable = false;
            $scope.enableDetailsSection = true;
            $scope.prograssBarVenue = false;
            $scope.prograssBarUpcoming = false;
            $scope.prograssBarArtists = false;
            $scope.enableResultTable = false;
        } else if (eventDetailsJSON == null || eventDetailsJSON == undefined || !eventDetailsJSON) {
            $scope.prograssBarEvent = false;
            $scope.enableProgress = false;
            $scope.enableDetailsButton = false;
            $scope.disableDetails = true;
            $scope.enableNoResults = false;
            $scope.enableFailedResults = false;
            $scope.enableFailedResultsForEvents = false;
            $scope.enableNoResultsForEvents = true;
            $scope.enableNoResultsForVenue = true
            $scope.enableNoResultsForArtists = true;
            $scope.enableNoResultsForUpcoming = true;
            $scope.enableResultTable = false;
            $scope.enableDetailsSection = true;
            $scope.prograssBarVenue = false;
            $scope.prograssBarUpcoming = false;
            $scope.prograssBarArtists = false;
            $scope.enableResultTable = false;
            //$scope.enableSecondPage = true;
        } else {
            var attractions = null;
            try {
                attractions = eventDetailsJSON._embedded.attractions;
            } catch (error) {
            }
            if (attractions != null) {
                $scope.artists = attractions.map(function (each) {
                    return each.name
                }).join(" | ");
            }
            $scope.prograssBarEvent = false;
            $scope.enableProgress = false;
            $scope.enableDetailsButton = false;
            $scope.enableNoResults = false;
            $scope.enableFailedResults = false;
            $scope.enableResultTable = false;
            $scope.enableFailedResultsForEvents = false;
            $scope.enableDetailsSection = true;
            $scope.enableNoResultsForEvents = false;
            //$scope.enableSecondPage = true;
        }

    }

    $scope.resetFailedAndNoResults = function() {
        $scope.enableNoResultsForEvents = false;
        $scope.enableNoResultsForVenue = false;
        $scope.enableNoResultsForArtists = false;
        $scope.enableNoResultsForUpcoming = false;
        $scope.enableFailedResultsForEvents = false;
        $scope.enableFailedResultsForVenue = false;
        $scope.enableFailedResultsForArtists = false;
        $scope.enableFailedResultsForUpcoming = false;
    }

    $scope.getMomentJSFormat = function(dateString) {
        return moment(dateString).format('MMM DD, YYYY');
    }

    $scope.getWarningClass = function (events) {
        if ($scope.currentTableModeForEvent == $scope.tableMode && events.id == $scope.selectedID ) {
            return 'table-warning';
        }
    }

    $scope.checkTwitterFavButton = function () {
        if(!eventDetailsJSON || eventDetailsJSON == undefined || !venueDetailsJSON || venueDetailsJSON == undefined) {
            return true;
        }
        return false;
    }

    $scope.getVenueInfo = function () {
        let venuename = "";
        try {venuename = eventDetailsJSON._embedded.venues[0].name} catch(error) {}
        venueDetailsJSON = "";

        if(!venuename || venuename == undefined){
            $scope.prograssBarVenue = false;
            $scope.enableNoResultsForVenue = true;
        } else {
            let venueData = {"venue": venuename};
            $http({
                method: 'GET',
                url: '/getVenueInfo',
                params: venueData
            }).then(function (response) {
                venueDetailsJSON = response.data;
                if (venueDetailsJSON == null || venueDetailsJSON == undefined || !venueDetailsJSON
                    || !venueDetailsJSON._embedded || !venueDetailsJSON._embedded.venues || venueDetailsJSON._embedded.venues.length <= 0) {
                    $scope.prograssBarVenue = false;
                    $scope.enableNoResultsForVenue = true;
                } else {
                    $scope.prograssBarVenue = false;
                    $scope.venueInfo = venueDetailsJSON._embedded.venues[0];
                    console.log(venueDetailsJSON);
                    setTimeout(function() { $scope.initMap();}, 500);
                }
                $scope.getUpcomingEventInfo();


            }, function (data) {
                $scope.prograssBarVenue = false;
                $scope.enableFailedResultsForVenue = true;
                $scope.getUpcomingEventInfo();
            });

        }
    }

    $scope.getUpcomingEventInfo = function() {
        let venueName = "";
        try {venueName = eventDetailsJSON._embedded.venues[0].name} catch(error) {}
        upcomingEventsJSON = ""

        if(!venueName || venueName == undefined){
            $scope.prograssBarUpcoming = false;
            $scope.enableNoResultsForUpcoming = true;
            $scope.googleCustomSearch();
        } else {
            let venueData = {"venue": venueName};
            $http({
                method: 'GET',
                url: '/getVenueIDSongkick',
                params: venueData
            }).then(function (response) {
                //$scope.initMap();
                try {upcomingEventsJSON = response.data} catch(error) {}
                if(upcomingEventsJSON && upcomingEventsJSON.resultsPage && upcomingEventsJSON.resultsPage.results && upcomingEventsJSON.resultsPage.results.event) {
                    $scope.prograssBarUpcoming = false;
                    console.log(upcomingEventsJSON);
                    $scope.upcomingInfo = upcomingEventsJSON.resultsPage.results.event;
                } else {
                    $scope.prograssBarUpcoming = false;
                    $scope.enableNoResultsForUpcoming = true;
                }
                //
                $scope.googleCustomSearch();

            }, function (data) {
                $scope.prograssBarUpcoming = false;
                $scope.enableFailedResultsForUpcoming = true;
                $scope.googleCustomSearch();
            });
        }
    }

    $scope.googleCustomSearch = function() {
        let artists = [];
        let artistNames = [];
        try { artists = eventDetailsJSON._embedded.attractions } catch(error) {}
        try { $scope.category = eventDetailsJSON.classifications[0].segment.name} catch(error) {}
        artistsJSON = [];

        if(artists && artists.length>0) {
            for(var i=0;i<artists.length;i++) {
                let name = "";
                try {name = artists[i].name; } catch(error) {}
                if(name && name != "Undefined") {
                    artistsJSON.push({"name": name});
                    artistNames.push(name);
                }
            }
            if(artistNames.length >0) {
                let requestName = {"artist": artistNames};
                $http({
                    method: 'GET',
                    url: '/customSearchImage',
                    params: requestName
                }).then(function (response) {
                    var result = [];
                    try {
                        result = response.data.googleResults;
                    } catch (error) {
                    }
                    googleSearchSuccess = !(result[0] == "nonefound" && result.every((val, i, arr) => val === arr[0]));
                    if (googleSearchSuccess) {
                        for (var i = 0; i < result.length; i++) {
                            artistsJSON[i]["googleCustom"] = result[i];
                        }
                    }
                    if ($scope.category == "Music" || $scope.category == "music") {
                        var finalResult = [];
                        $scope.getArtistInfo(artistNames, finalResult, 0);
                    } else {
                        if (!googleSearchSuccess) {
                            $scope.prograssBarArtists = false;
                            $scope.enableFailedResultsForArtists = true;
                        } else {
                            console.log(artistsJSON);
                            $scope.prograssBarArtists = false;
                            $scope.artistsInfo = artistsJSON;
                        }
                    }
                }, function (data) {
                    $scope.prograssBarArtists = false;
                    console.log(data);
                });
            } else {
                $scope.prograssBarArtists = false;
                $scope.enableNoResultsForArtists = true;

            }
        } else {
            $scope.prograssBarArtists = false;
            $scope.enableNoResultsForArtists = true;
        }
    }


    $scope.getArtistInfo = function (artistNames,results,index) {
        if(index+1 <= artistNames.length) {
            let reqName = {"artists": artistNames[index]};
            $http({
                method: 'GET',
                url: '/getArtistInfoSpotify',
                params: reqName
            }).then(function(response) {
                let result;
                try {result = response.data.spotifyResults} catch(error) {results.push("nonefound")}
                let correctResult;
                for(var i=0; i<result.length;i++) {
                    let name = result[i].name;
                    if(name.toLowerCase() == artistNames[index].toLowerCase()) {
                        correctResult = result[i];
                        break;
                    }
                }
                results.push(correctResult);
                $scope.getArtistInfo(artistNames,results,index+1);
                if(results.length == artistNames.length) {
                    spotifySearchSuccess = !(results[0] == "nonefound" && results.every( (val, i, arr) => val === arr[0] ));
                    if(spotifySearchSuccess) {
                        for (var i = 0; i < results.length; i++) {
                            artistsJSON[i]["spotifyResults"] = results[i];
                        }
                    }
                    if(!spotifySearchSuccess && !googleSearchSuccess) {
                        $scope.prograssBarArtists = false;
                        $scope.enableFailedResultsForArtists = true;
                    } else {
                        console.log(artistsJSON);
                        $scope.prograssBarArtists = false;
                        $scope.artistsInfo = artistsJSON;
                    }
                }
            }, function(data) {
                results.push("nonefound");
            })

        } else {
            return;
        }
    }

    $scope.removeWarning = function (event) {
        var element = document.getElementsByClassName('table-warning');
        if(element.length != 0 && event.id == $scope.selectedID) {
            warnedElem = element[0];
            element[0].classList.remove("table-warning");
        }
    }

    $scope.addWarning = function () {
        if(warnedElem != null) {
            warnedElem.classList.add("table-warning");
            warnedElem = null;
        }
    }

    $scope.formatFollowers = function (number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    $scope.postTweet = function () {
        var eventName = "N/A";
        try {eventName = eventDetailsJSON.name} catch(error) {}
        var venueLocation = "N/A";
        try {venueLocation = venueDetailsJSON._embedded.venues[0].name} catch(error) {}
        var website = "N/A"
        try {website = eventDetailsJSON.url} catch(error) {}
        var tweetMessage = "Check out "+ eventName + " located at " + venueLocation + ". Website: " + website + " #CSCI571EventSearch";
        var tweetUrl = "https://twitter.com/intent/tweet?text=" + encodeURIComponent(tweetMessage);
        window.open(tweetUrl,'_blank');
    }

    $scope.upcomingViewOptionChanged = function (lasttab) {
        var selection = lasttab.upcomingOptions;

        if(selection == 'default') {
            $scope.upcomingInfo = upcomingEventsJSON.resultsPage.results.event;
            return;
        }
        var sortOption = lasttab.sortOrder;
        var originalData = (upcomingEventsJSON.resultsPage.results.event).slice();
        if(selection == 'eventName') {
            if(sortOption == 'ascend') {
                var sorted_result = originalData.sort(function (a, b) {
                    let name1 = a.displayName;
                    let name2 = b.displayName;
                    return (name1 > name2) - (name1 < name2)
                });
            } else {
                var sorted_result = originalData.sort(function (a, b) {
                    let name1 = a.displayName;
                    let name2 = b.displayName;
                    return (name2 > name1) - (name2 < name1)
                });
            }
            $scope.upcomingInfo = sorted_result;
        } else if(selection=='time') {
            if (sortOption == 'ascend') {
                var sorted_result = originalData.sort(function (a, b) {
                    let date1 = new Date(a.start.date);
                    let date2 = new Date(b.start.date);

                    return date1 - date2;
                });

                var sorted_result_final = sorted_result.sort(function (a, b) {
                    let time1 = new Date(a.start.time);
                    let time2 = new Date(b.start.time);

                    return time1 - time2;
                });
                $scope.upcomingInfo = sorted_result_final;
            } else {
                var sorted_result = originalData.sort(function (a, b) {
                    let date1 = new Date(a.start.date);
                    let date2 = new Date(b.start.date);

                    return date2 - date1;
                });

                var sorted_result_final = sorted_result.sort(function (a, b) {
                    let time1 = new Date(a.start.time);
                    let time2 = new Date(b.start.time);

                    return time2 - time1;
                });
                $scope.upcomingInfo = sorted_result_final;
            }
        }else if(selection=='artist') {
            if(sortOption == 'ascend') {
                var sorted_result = originalData.sort(function (a, b) {
                    let name1 = a.performance[0].artist.displayName;
                    let name2 = b.performance[0].artist.displayName;
                    return (name1 > name2) - (name1 < name2)
                });
            } else {
                var sorted_result = originalData.sort(function (a, b) {
                    let name1 = a.performance[0].artist.displayName;
                    let name2 = b.performance[0].artist.displayName;
                    return (name2 > name1) - (name2 < name1)
                });
            }
            $scope.upcomingInfo = sorted_result;
        } else if(selection=='type') {
            if(sortOption == 'ascend') {
                var sorted_result = originalData.sort(function (a, b) {
                    let name1 = a.type;
                    let name2 = b.type;
                    return (name1 > name2) - (name1 < name2)
                });
            } else {
                var sorted_result = originalData.sort(function (a, b) {
                    let name1 = a.type;
                    let name2 = b.type;
                    return (name2 > name1) - (name2 < name1)
                });
            }
            $scope.upcomingInfo = sorted_result;
        }
    }
    $scope.$watch('show_cards', function(){
        $scope.buttonText = $scope.show_cards ? 'Show Less' : 'Show More';
    })

    $scope.toggleShowCards = function () {
        if($scope.show_cards == false) {
            $scope.show_cards = true;
        } else{
            $scope.show_cards = false;
        }
    }
});