const express = require('express');
const geohash = require('ngeohash');
const path = require('path')
const bodyParser = require('body-parser');
const request = require('request');
const promiseReq = require('request-promise');
const SpotifyWebAPI = require('spotify-web-api-node');

const TICKETMASTER_API_KEY = "Buwj0G2A846oGezBw3N05bgYt9YgUx5s";
const GOOGLE_API_KEY = "AIzaSyDSvNSOBQhqi-J0jNCHQeYT60p9ES4OLig";
const SPOTIFY_CLIENT_ID = "daa4c39dc189407084bdbba3e64a7517";
const SPOTIFY_SECRET = "2131616c78ac4664b2394351673bd262";
const GOOGLE_CUSTOM_SEARCH_KEY = "AIzaSyAd7AWR3GZaxrkSMCS7k09aTQsFFIn6PIo";
const GOOGLE_SEARCH_ENGINE_ID = "011142998701674357804:u8bdjz-s64k";
const SONGKICK_API_KEY = "QoIMKkvzOECUmf1W";

const spotifyApi = new SpotifyWebAPI({
    clientId: SPOTIFY_CLIENT_ID,
    clientSecret: SPOTIFY_SECRET
});

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "app")));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get("/",function(req,res){
    res.redirect("indexTest.html");
});

app.get('/getVenueIDSongkick', async function(req, res) {
    var venueName = req.query.venue;
    var songKickVenueIDURL = "https://api.songkick.com/api/3.0/search/venues.json?query="+ encodeURIComponent(venueName)+"&apikey="+SONGKICK_API_KEY;
    request(songKickVenueIDURL, function (error, response, body) {
        if (error) {
            res.sendStatus(503);
        } else {
            if (response.statusCode == 200) {
                var venueJSON = JSON.parse(body);
                var id;
                try {id = venueJSON.resultsPage.results.venue[0].id; } catch(error) {}
                let upcomingEventURL = "https://api.songkick.com/api/3.0/venues/"+id+"/calendar.json?apikey="+SONGKICK_API_KEY;
                request(upcomingEventURL, function (errorUp, responseUp, bodyUp) {
                    if (errorUp) {
                        res.sendStatus(503);
                    } else {
                        if (responseUp.statusCode == 200) {
                            let upcomingEventJSON = JSON.parse(bodyUp);
                            res.json(upcomingEventJSON);
                        } else {
                            res.sendStatus(responseUp.statusCode);
                        }
                    }
                });
            } else {
                res.sendStatus(response.statusCode);
            }
        }
    });
});

app.get('/getArtistInfoSpotify', async function(req, res) {
    var artistName = req.query.artists;
    var result;

    if (!artistName || artistName == "Undefined") {
        result = "nonefound";
        res.json({"spotifyResults": result});
    } else {
        spotifyApi.searchArtists(artistName).then(function(data) {
            var out = data.body.artists.items;
            result= out;
            res.json({"spotifyResults": result});
        },function(error) {
            if(error.statusCode == 401) {
                spotifyApi.clientCredentialsGrant().then(
                    function(data) {
                        spotifyApi.setAccessToken(data.body['access_token']);
                        spotifyApi.searchArtists(artistName).then(function(data) {
                            var out = data.body.artists.items;
                            result = out;
                            res.json({"spotifyResults": result});
                        }, function (er) {
                            result = "nonefound";
                            res.json({"spotifyResults": result});
                        });
                    },
                    function(err) {
                        result = "nonefound";
                        res.json({"spotifyResults": result});
                    }
                );
            }else {
                result="nonefound";
                res.json({"spotifyResults": result});
            }
        });
    }

});

app.get('/customSearchImage',async function(req,res) {
    var artistArray = req.query.artist;
    if(!Array.isArray(artistArray)) {
        let str = artistArray;
        artistArray = [];
        artistArray.push(str)
    }
    var result = [];
    for(var i=0; i< artistArray.length; i++) {
        var name = artistArray[i];
        if(!name || name == "Undefined") {
            result.push("nonefound");
        } else {
            var artistImageFetchURL = "https://www.googleapis.com/customsearch/v1?q=" + encodeURIComponent(name) + "&cx="+GOOGLE_SEARCH_ENGINE_ID+"&imgSize=huge&imgType=news&num=9&searchType=image&key="+GOOGLE_CUSTOM_SEARCH_KEY;
            try {
                var fetchResult = await promiseReq(artistImageFetchURL);
                var artistData = JSON.parse(fetchResult);
                result.push(artistData)
            } catch(error) {
                result.push("nonefound");
            }
        }
    }
    res.json({"googleResults":result});

});

app.get('/getVenueInfo', async function(req,res) {
    var venueName = req.query.venue;
    var venueFetchURL = "https://app.ticketmaster.com/discovery/v2/venues.json?apikey=" + TICKETMASTER_API_KEY +"&keyword="+ encodeURIComponent(venueName);
    setTimeout(function() {
        request(venueFetchURL, function (error, response, body) {
            if (error) {
                res.sendStatus(503);
            } else {
                if (response.statusCode == 200) {
                    var venueJSON = JSON.parse(body);
                    res.json(venueJSON);
                } else {
                    res.sendStatus(response.statusCode);
                }
            }
        });
    }, 700);
});

app.get('/fetchEventDetails',async function(req, res) {
    var eventID = req.query.eventID;
    var detailsReqUrl = "https://app.ticketmaster.com/discovery/v2/events/"+ eventID + ".json?apikey="+TICKETMASTER_API_KEY;
    setTimeout(function() { request(detailsReqUrl,function(error,response,body) {
        if (error) {
            res.sendStatus(503);
        } else {
            if (response.statusCode == 200) {
                var detailsJSON = JSON.parse(body);
                res.json(detailsJSON);
            } else {
                res.sendStatus(response.statusCode);
            }
        }
    });
    }, 700);
});

app.get('/eventSearch', async function(req,res) {
    var keyword = req.query.keyword;
    var category = req.query.category;
    var distanceUnit = req.query.distance_unit;
    var radioButton = req.query.radioButton;
    var distance = req.query.distance;
    var location = null;
    try { location = JSON.parse(req.query.currentLocation); } catch(error) {console.log("current location JSON parse failed")}
    var locationName = req.query.locationInput;
    var sourceLat = null;
    var sourceLong = null;
    var hash = "";
    var segmentID = "";

    if(distance == undefined) {
        distance = 10;
    }

    if(radioButton == "here") {
        sourceLat = location.lat;
        sourceLong = location.lon;
        hash = geohash.encode(sourceLat,sourceLong);
    } else {
        var googleMapReqUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURIComponent(locationName) + "&key=" + GOOGLE_API_KEY;
        try{
            let response = await promiseReq(googleMapReqUrl);
            let locationJSON = JSON.parse(response);
            sourceLat = parseFloat(locationJSON.results[0].geometry.location.lat);
            sourceLong = parseFloat(locationJSON.results[0].geometry.location.lng);
            hash = geohash.encode(sourceLat,sourceLong);
        } catch(error){}
    }
    if (category == "default") {
        segmentID = "";
    } else if (category == "music") {
        segmentID = "KZFzniwnSyZfZ7v7nJ";
    } else if (category == "sports") {
        segmentID = "KZFzniwnSyZfZ7v7nE";
    } else if (category == "arts") {
        segmentID = "KZFzniwnSyZfZ7v7na";
    } else if (category == "film") {
        segmentID = "KZFzniwnSyZfZ7v7nn";
    } else {
        segmentID = "KZFzniwnSyZfZ7v7n1";
    }

    let ticketMasterURL = "https://app.ticketmaster.com/discovery/v2/events.json?apikey=" + TICKETMASTER_API_KEY + "&keyword=" + encodeURIComponent(keyword) + "&segmentId=" + segmentID + "&radius=" + distance + "&unit="+ distanceUnit +"&geoPoint="+ hash;
    setTimeout(function() {request(ticketMasterURL,function(error,response,body) {
        if (error) {
            res.sendStatus(503);
        } else {
            if (response.statusCode == 200) {
                var eventDetailsJSON = JSON.parse(body);
                eventDetailsJSON.sourceLat = sourceLat;
                eventDetailsJSON.sourceLong = sourceLong;
                res.json(eventDetailsJSON);
            } else {
                res.sendStatus(response.statusCode);
            }
        }
    })}, 700);

});

app.get('/autoComplete',async function(req, res) {
    var requestData = req.query.keyword;
    var auto_url = "https://app.ticketmaster.com/discovery/v2/suggest?apikey="+TICKETMASTER_API_KEY+"&keyword="+encodeURIComponent(requestData);

    request(auto_url,function(error,response,body){
        if(error){
            res.sendStatus(503);
        }else{
            if(response.statusCode == 200){
                var suggestionJSON = JSON.parse(body);
                res.json(suggestionJSON);
            } else {
                res.sendStatus(response.statusCode);
            }
        }
    })
});

app.listen(process.env.PORT || 8000, function(){
    console.log("ESA Server Listening");
});
