package com.example.deepa.entsearchapp;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class AutoCompleteAPI {

    private static AutoCompleteAPI mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    public AutoCompleteAPI(Context ctx) {
        mCtx = ctx;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized AutoCompleteAPI getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AutoCompleteAPI(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public static void make(Context ctx, String query, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        String url = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/autoComplete?keyword=";
        try {
            url = url + URLEncoder.encode(query, "UTF-8");
        } catch(UnsupportedEncodingException error) {
            error.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, listener, errorListener);
        AutoCompleteAPI.getInstance(ctx).addToRequestQueue(stringRequest);
    }
}
