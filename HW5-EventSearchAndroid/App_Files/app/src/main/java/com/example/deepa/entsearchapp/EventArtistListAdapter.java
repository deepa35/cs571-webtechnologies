package com.example.deepa.entsearchapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

public class EventArtistListAdapter extends RecyclerView.Adapter<EventArtistListAdapter.ImageViewHolderArt> {
    private ArrayList<String> imgDataset;
    private LayoutInflater viewInflater;
    private Context context;
    private ImageLoader mImageLoader;


    public EventArtistListAdapter(ArrayList<String> imgDataset, Context context) {
        this.imgDataset = imgDataset;
        viewInflater = LayoutInflater.from(context);
        this.context = context;
        mImageLoader = VolleyImageLoader.getInstance(context).getImageLoader();
    }

    public static class ImageViewHolderArt extends RecyclerView.ViewHolder {
        NetworkImageView artistImg;

        ImageViewHolderArt(View eachImg) {
            super(eachImg);
            artistImg = (NetworkImageView)eachImg.findViewById(R.id.eachImageArt);
        }
    }

    @Override
    public ImageViewHolderArt onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = viewInflater.inflate(R.layout.image_each_artist, parent, false);
        ImageViewHolderArt viewHolder = new ImageViewHolderArt(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ImageViewHolderArt holder, final int position) {
        mImageLoader.get(imgDataset.get(position), ImageLoader.getImageListener(holder.artistImg, R.drawable.empty_rectangle, R.drawable.empty_rectangle_error));
        holder.artistImg.setImageUrl(imgDataset.get(position), mImageLoader);
    }

    @Override
    public int getItemCount() {
        if(imgDataset == null) {
            return 0;
        }
        return imgDataset.size();
    }
}
