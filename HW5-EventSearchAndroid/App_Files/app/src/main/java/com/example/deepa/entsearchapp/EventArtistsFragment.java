package com.example.deepa.entsearchapp;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class EventArtistsFragment extends Fragment {
    private View artistsView;
    private String[] artists;
    private ArrayList<String> artist1GooglePics;
    private ArrayList<String> artist2GooglePics;
    private boolean isArtist1PicsAvailable;
    private boolean isArtist2PicsAvailable;

    private LinearLayout progressBarArtist;
    private TextView noArtistResults;
    private LinearLayout artistsDataHolder;

    //Artist1 elements
    private TextView artist1Name;
    private TableLayout artist1Table;
    private TableRow artist1NameRow;
    private TableRow artist1FollowerRow;
    private TableRow artist1PopRow;
    private TableRow artist1SpotifyRow;
    private RecyclerView artist1Pics;
    private RecyclerView.Adapter artist1Adapter;
    private RecyclerView.LayoutManager artist1LayoutManager;

    //Artist1 elements
    private TextView artist2Name;
    private TableLayout artist2Table;
    private TableRow artist2NameRow;
    private TableRow artist2FollowerRow;
    private TableRow artist2PopRow;
    private TableRow artist2SpotifyRow;
    private RecyclerView artist2Pics;
    private RecyclerView.Adapter artist2Adapter;
    private RecyclerView.LayoutManager artist2LayoutManager;

    public EventArtistsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        artistsView = inflater.inflate(R.layout.fragment_event_artists, container, false);

        progressBarArtist = artistsView.findViewById(R.id.progress_bar_artist);
        noArtistResults = artistsView.findViewById(R.id.no_artist_results);
        artistsDataHolder = artistsView.findViewById(R.id.artistsDataHolder);

        //Artist1
        artist1Name = artistsView.findViewById(R.id.artist1Title);
        artist1Table = artistsView.findViewById(R.id.artist1Table);
        artist1NameRow = artistsView.findViewById(R.id.artist1NameRow);
        artist1FollowerRow = artistsView.findViewById(R.id.artist1FollowRow);
        artist1PopRow = artistsView.findViewById(R.id.artist1PopRow);
        artist1SpotifyRow = artistsView.findViewById(R.id.artist1CheckRow);
        artist1Pics = artistsView.findViewById(R.id.artist1Pics);

        //Artist2
        artist2Name = artistsView.findViewById(R.id.artist2Title);
        artist2Table = artistsView.findViewById(R.id.artist2Table);
        artist2NameRow = artistsView.findViewById(R.id.artist2NameRow);
        artist2FollowerRow = artistsView.findViewById(R.id.artist2FollowRow);
        artist2PopRow = artistsView.findViewById(R.id.artist2PopRow);
        artist2SpotifyRow = artistsView.findViewById(R.id.artist2CheckRow);
        artist2Pics = artistsView.findViewById(R.id.artist2Pics);

        progressBarArtist.setVisibility(View.VISIBLE);
        noArtistResults.setVisibility(View.GONE);
        artistsDataHolder.setVisibility(View.GONE);
        artist1Table.setVisibility(View.GONE);
        artist2Table.setVisibility(View.GONE);

        artists = EventDetailsActivity.selectedEvent.getEventArtists();

        if (artists == null || artists.length == 0) {
            progressBarArtist.setVisibility(View.GONE);
            noArtistResults.setVisibility(View.VISIBLE);
            artistsDataHolder.setVisibility(View.GONE);
        } else {
            artistsDataHolder.setVisibility(View.VISIBLE);
            if(artists.length == 1) {
                artist1Name.setText(artists[0]);
                artist2Name.setVisibility(View.GONE);
            } else {
                artist1Name.setText(artists[0]);
                artist2Name.setText(artists[1]);
            }

            String googleCustomURL = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/customSearchImage?artist=";
            try {
                googleCustomURL +=  URLEncoder.encode(artists[0],"UTF-8");
                if (artists.length == 2) {
                    googleCustomURL += "&artist=" + URLEncoder.encode(artists[1],"UTF-8");
                }
            } catch(UnsupportedEncodingException error) {
                error.printStackTrace();
            }
            RequestQueue queue = Volley.newRequestQueue(getActivity());

            StringRequest stringRequest = new StringRequest(Request.Method.GET, googleCustomURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        EventDetailsActivity.eventArtistGoogleObj = new JSONObject(response);
                        EventDetailsActivity.artistEventIdentifier = EventDetailsActivity.selectedEvent.getEventID();

                        if(artists.length >= 1) {
                            JSONArray artist1Arr = (JSONArray) EventDetailsActivity.eventArtistGoogleObj.get("artist1");
                            artist1GooglePics = new ArrayList<>(JSONArrayToStrArray(artist1Arr));
                        }

                        if(artists.length >=2) {
                            JSONArray artist2Arr = (JSONArray) EventDetailsActivity.eventArtistGoogleObj.get("artist2");
                            artist2GooglePics = new ArrayList<>(JSONArrayToStrArray(artist2Arr));
                        }
                        progressBarArtist.setVisibility(View.GONE);
                        populateImages(artist1GooglePics, artist2GooglePics);

                        if(!isMusic()) {
                            artist1Table.setVisibility(View.GONE);
                            artist2Table.setVisibility(View.GONE);
                            if(artists.length == 1) {
                                artist2Pics.setVisibility(View.GONE);
                                if(!isArtist1PicsAvailable) {
                                    artist1Name.setVisibility(View.GONE);
                                    noArtistResults.setVisibility(View.VISIBLE);
                                }
                            }

                            if(artists.length ==2) {
                                if(isArtist1PicsAvailable && !isArtist2PicsAvailable) {
                                    artist2Name.setVisibility(View.GONE);
                                } else if(!isArtist1PicsAvailable && isArtist2PicsAvailable) {
                                    artist1Name.setVisibility(View.GONE);
                                } else if (!isArtist1PicsAvailable && !isArtist2PicsAvailable) {
                                    artist1Name.setVisibility(View.GONE);
                                    artist2Name.setVisibility(View.GONE);
                                    noArtistResults.setVisibility(View.VISIBLE);
                                }
                            }

                        } else {
                            sendRequestSpotifyArtist1();
                        }

                    } catch (JSONException error) {
                        error.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(!isMusic()) {
                        progressBarArtist.setVisibility(View.GONE);
                        noArtistResults.setVisibility(View.VISIBLE);
                        artistsDataHolder.setVisibility(View.GONE);
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), "Failed to get artist information", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        isArtist1PicsAvailable = false;
                        artist1Pics.setVisibility(View.GONE);
                        isArtist2PicsAvailable = false;
                        artist2Pics.setVisibility(View.GONE);
                        sendRequestSpotifyArtist1();
                    }
                }
            });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);
        }
        return artistsView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void populateImages(ArrayList<String> artist1Images, ArrayList<String> artist2Images){
        if(artists.length >= 1) {
            if(artist1Images.size() == 1 && artist1Images.get(0).equals("null")) {
                isArtist1PicsAvailable = false;
                artist1Pics.setVisibility(View.GONE);
            } else {
                isArtist1PicsAvailable = true;
                artistsDataHolder.setVisibility(View.VISIBLE);
                noArtistResults.setVisibility(View.GONE);
                artist1LayoutManager = new LinearLayoutManager(getActivity());
                artist1Pics.setVisibility(View.VISIBLE);
                artist1Pics.setLayoutManager(artist1LayoutManager);
                artist1Pics.setHasFixedSize(true);
                artist1Pics.setNestedScrollingEnabled(false);
                artist1Adapter = new EventArtistListAdapter(artist1Images, getActivity());
                artist1Pics.setAdapter(artist1Adapter);
            }
        }
        if(artists.length >= 2) {
            if(artist2Images.size() == 1 && artist2Images.get(0).equals("null")) {
                isArtist2PicsAvailable = false;
                artist2Pics.setVisibility(View.GONE);
            } else {
                isArtist2PicsAvailable = true;
                artistsDataHolder.setVisibility(View.VISIBLE);
                noArtistResults.setVisibility(View.GONE);
                artist2Name.setText(artists[1]);
                artist2LayoutManager = new LinearLayoutManager(getActivity());
                artist2Pics.setVisibility(View.VISIBLE);
                artist2Pics.setLayoutManager(artist2LayoutManager);
                artist2Pics.setHasFixedSize(true);
                artist2Pics.setNestedScrollingEnabled(false);
                artist2Adapter = new EventArtistListAdapter(artist2Images, getActivity());
                artist2Pics.setAdapter(artist2Adapter);
            }
        }
    }

    private void sendRequestSpotifyArtist1() {
        artists = EventDetailsActivity.selectedEvent.getEventArtists();

        String spotifyReqArt1 = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/getArtistInfoSpotify?artists=";
        try {
            spotifyReqArt1=spotifyReqArt1+URLEncoder.encode(artists[0], "UTF-8");
        } catch(UnsupportedEncodingException error) {
            error.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, spotifyReqArt1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    EventDetailsActivity.eventArt1SpotifyObj = new JSONObject(response);
                    if(EventDetailsActivity.eventArt1SpotifyObj.length() == 0) {
                        artist1Table.setVisibility(View.GONE);
                        if(artists.length == 1) {
                            progressBarArtist.setVisibility(View.GONE);
                            artist2Table.setVisibility(View.GONE);
                            artist2Pics.setVisibility(View.GONE);
                            if(!isArtist1PicsAvailable) {
                                artist1Name.setVisibility(View.GONE);
                                noArtistResults.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if(!isArtist1PicsAvailable) {
                                artist1Name.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        progressBarArtist.setVisibility(View.GONE);
                        artist1Table.setVisibility(View.VISIBLE);
                        populateSpotifyTableArtist1(EventDetailsActivity.eventArt1SpotifyObj);
                        if(artists.length == 1) {
                            artist2Table.setVisibility(View.GONE);
                            artist2Pics.setVisibility(View.GONE);
                        }
                    }
                    if(artists.length == 2) {
                        sendRequestSpotifyArtist2();
                    }
                } catch (JSONException error) {
                    error.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                artist1Table.setVisibility(View.GONE);
                if(artists.length == 1) {
                    progressBarArtist.setVisibility(View.GONE);
                    artist2Pics.setVisibility(View.GONE);
                    artist2Table.setVisibility(View.GONE);
                    if(!isArtist1PicsAvailable) {
                        artist1Name.setVisibility(View.GONE);
                        noArtistResults.setVisibility(View.VISIBLE);
                    }
                } else {
                    if(!isArtist1PicsAvailable) {
                        artist1Name.setVisibility(View.GONE);
                    }
                    sendRequestSpotifyArtist2();
                }
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void sendRequestSpotifyArtist2() {
        artists = EventDetailsActivity.selectedEvent.getEventArtists();

        String spotifyReqArt2 = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/getArtistInfoSpotify?artists=";
        try {
            spotifyReqArt2=spotifyReqArt2+URLEncoder.encode(artists[1], "UTF-8");
        } catch(UnsupportedEncodingException error) {
            error.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, spotifyReqArt2, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progressBarArtist.setVisibility(View.GONE);
                    EventDetailsActivity.eventArt2SpotifyObj = new JSONObject(response);

                    if(EventDetailsActivity.eventArt2SpotifyObj.length() == 0) {
                        artist2Table.setVisibility(View.GONE);
                        if(!isArtist2PicsAvailable) {
                            artist2Name.setVisibility(View.GONE);
                            artist2Pics.setVisibility(View.GONE);
                        }

                        if(!isArtist1PicsAvailable && EventDetailsActivity.eventArt1SpotifyObj.length() == 0) {
                            noArtistResults.setVisibility(View.VISIBLE);
                        }
                    } else {
                        artist2Table.setVisibility(View.VISIBLE);
                        populateSpotifyTableArtist2(EventDetailsActivity.eventArt2SpotifyObj);
                        if(!isArtist2PicsAvailable) {
                            artist2Pics.setVisibility(View.GONE);
                        }
                    }

                } catch (JSONException error) {
                    error.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBarArtist.setVisibility(View.GONE);
                artist2Table.setVisibility(View.GONE);
                if(!isArtist2PicsAvailable) {
                    artist2Name.setVisibility(View.GONE);
                    artist2Pics.setVisibility(View.GONE);
                }
                if(!isArtist1PicsAvailable && EventDetailsActivity.eventArt1SpotifyObj.length() == 0) {
                    noArtistResults.setVisibility(View.VISIBLE);
                }
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

    private void populateSpotifyTableArtist1(JSONObject responseObj) throws JSONException {
        String name = responseObj.get("name").toString();
        String followers = responseObj.get("follow").toString();
        String popularity = responseObj.get("popular").toString();
        String spotifyURL = responseObj.get("url").toString();

        if(name.equals("null")) {
            artist1NameRow.setVisibility(View.GONE);
        } else {
            ((TextView) artistsView.findViewById(R.id.artist1NameRowVal)).setText(name);
            artist1NameRow.setVisibility(View.VISIBLE);
        }

        if(followers.equals("null")) {
            artist1FollowerRow.setVisibility(View.GONE);
        } else {
            ((TextView) artistsView.findViewById(R.id.artist1FollowRowVal)).setText(followers);
            artist1FollowerRow.setVisibility(View.VISIBLE);
        }

        if(popularity.equals("null")) {
            artist1PopRow.setVisibility(View.GONE);
        } else {
            ((TextView) artistsView.findViewById(R.id.artist1PopRowVal)).setText(popularity);
            artist1PopRow.setVisibility(View.VISIBLE);
        }

        if (spotifyURL.equals("null")) {
            artist1SpotifyRow.setVisibility(View.GONE);
        } else {
            TextView txt = (TextView) artistsView.findViewById(R.id.artist1CheckRowVal);
            String textVal = "<a href='" + spotifyURL + "'>Spotify</a>";
            if (Build.VERSION.SDK_INT < 24) {
                txt.setText(Html.fromHtml(textVal));
            } else {
                txt.setText(Html.fromHtml(textVal, Html.FROM_HTML_MODE_COMPACT));
            }
            txt.setClickable(true);
            txt.setMovementMethod(LinkMovementMethod.getInstance());
            artist1SpotifyRow.setVisibility(View.VISIBLE);
        }
    }

    private void populateSpotifyTableArtist2(JSONObject responseObj) throws JSONException {
        String name = responseObj.get("name").toString();
        String followers = responseObj.get("follow").toString();
        String popularity = responseObj.get("popular").toString();
        String spotifyURL = responseObj.get("url").toString();

        if(name.equals("null")) {
            artist2NameRow.setVisibility(View.GONE);
        } else {
            ((TextView) artistsView.findViewById(R.id.artist2NameRowVal)).setText(name);
            artist2NameRow.setVisibility(View.VISIBLE);
        }

        if(followers.equals("null")) {
            artist2FollowerRow.setVisibility(View.GONE);
        } else {
            ((TextView) artistsView.findViewById(R.id.artist2FollowRowVal)).setText(followers);
            artist2FollowerRow.setVisibility(View.VISIBLE);
        }

        if(popularity.equals("null")) {
            artist2PopRow.setVisibility(View.GONE);
        } else {
            ((TextView) artistsView.findViewById(R.id.artist2PopRowVal)).setText(popularity);
            artist2PopRow.setVisibility(View.VISIBLE);
        }

        if (spotifyURL.equals("null")) {
            artist2SpotifyRow.setVisibility(View.GONE);
        } else {
            TextView txt = (TextView) artistsView.findViewById(R.id.artist2CheckRowVal);
            String textVal = "<a href='" + spotifyURL + "'>Spotify</a>";
            if (Build.VERSION.SDK_INT < 24) {
                txt.setText(Html.fromHtml(textVal));
            } else {
                txt.setText(Html.fromHtml(textVal, Html.FROM_HTML_MODE_COMPACT));
            }
            txt.setClickable(true);
            txt.setMovementMethod(LinkMovementMethod.getInstance());
            artist2SpotifyRow.setVisibility(View.VISIBLE);
        }
    }

    private ArrayList<String> JSONArrayToStrArray(JSONArray array) throws JSONException {
        ArrayList<String> stringList = new ArrayList<>();
        for (int i=0; i<array.length(); i++) {
            stringList.add(array.get(i).toString() );
        }
        return stringList;
    }

    private boolean isMusic() {
        if (EventDetailsActivity.selectedEvent.getEventCategory().equalsIgnoreCase("music")) {
            return true;
        }
        return false;
    }
}
