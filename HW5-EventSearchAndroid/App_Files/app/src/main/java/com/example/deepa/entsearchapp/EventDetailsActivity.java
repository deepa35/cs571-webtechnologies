package com.example.deepa.entsearchapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class EventDetailsActivity extends AppCompatActivity {
    private static int NUM_TABS = 4;
    private EventsDetailPagerAdaptor mEventDetailsPagerAdaptor;
    private ViewPager mViewPager;
    private TabLayout layout;
    private FavoritesManager favManager;
    private MenuItem fav_icon;
    private EventArtistsFragment artist;
    public static EventListData selectedEvent;
    public static EventDetailsData selectedEventInfo;

    public static JSONObject eventInfoObject;
    public static JSONObject eventVenueObject;
    public static JSONObject eventArtistGoogleObj;
    public static JSONObject eventArt1SpotifyObj;
    public static JSONObject eventArt2SpotifyObj;
    public static JSONArray eventUpcomingObj;

    public static String venueEventIdentifier;
    public static String artistEventIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        favManager = new FavoritesManager();
        String requestStr = (String)getIntent().getExtras().getSerializable("eventData");
        selectedEvent = new Gson().fromJson(requestStr, EventListData.class);
        venueEventIdentifier = "";
        artistEventIdentifier = "";

        setContentView(R.layout.activity_event_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(selectedEvent.getEventName());

        mViewPager = (ViewPager) findViewById(R.id.details_view_pager);
        mEventDetailsPagerAdaptor = new EventsDetailPagerAdaptor(getSupportFragmentManager());
        mViewPager.setAdapter(mEventDetailsPagerAdaptor);
        mViewPager.setOffscreenPageLimit(3);
        layout = (TabLayout) findViewById(R.id.details_tablayout);
        layout.setupWithViewPager(mViewPager);
        customiseTabs();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.fav_icon_withindetails:
                String toastMessage = "";
                if(favManager.isInFavorites(selectedEvent,MainActivity.getContext())) {
                    favManager.removeFromFavorite(MainActivity.getContext(), selectedEvent);
                    fav_icon.setIcon(R.drawable.heart_fill_white);
                    toastMessage = toastMessage + selectedEvent.getEventName() + " was removed from favorites";
                } else {
                    favManager.addToFavorite(MainActivity.getContext(), selectedEvent);
                    fav_icon.setIcon(R.drawable.heart_fill_red);
                    toastMessage = toastMessage + selectedEvent.getEventName() + " was added to favorites";
                }
                Toast.makeText(this,toastMessage,Toast.LENGTH_SHORT).show();
                return true;
            case R.id.twitter_menu:
                if(selectedEvent != null && selectedEventInfo!= null) {
                    String twitterMessage = "Check out " + selectedEvent.getEventName() + " located at " + selectedEvent.getEventVenue()
                            + ". Website: " + selectedEventInfo.getTicketPurchaseURL()+ " #CSCI571EventSearch";
                    String twitterURL = "";
                    try {
                        twitterURL = "https://twitter.com/intent/tweet?text=" + URLEncoder.encode(twitterMessage, "UTF-8");
                    } catch(UnsupportedEncodingException error) {
                        return false;
                    }
                    Intent twitter = new Intent(Intent.ACTION_VIEW);
                    twitter.setData(Uri.parse(twitterURL));
                    startActivity(twitter);
                }

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.fav_twitter_menu, menu);
        fav_icon = menu.findItem(R.id.fav_icon_withindetails);
        if(favManager.isInFavorites(selectedEvent,MainActivity.getContext())) {
            fav_icon.setIcon(R.drawable.heart_fill_red);
        } else {
            fav_icon.setIcon(R.drawable.heart_fill_white);
        }
        return true;
    }


    private void customiseTabs() {
        TextView tabInfo = (TextView) LayoutInflater.from(this).inflate(R.layout.event_details_tab, null);
        tabInfo.setText("EVENT");
        tabInfo.setTextColor(this.getResources().getColorStateList(R.color.tab_text_color));
        tabInfo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.info_outline, 0, 0);
        layout.getTabAt(0).setCustomView(tabInfo);

        TextView tabArtists = (TextView) LayoutInflater.from(this).inflate(R.layout.event_details_tab, null);
        tabArtists.setText("ARTIST(S)");
        tabArtists.setTextColor(this.getResources().getColorStateList(R.color.tab_text_color));
        tabArtists.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.artist, 0, 0);
        layout.getTabAt(1).setCustomView(tabArtists);

        TextView tabVenue = (TextView) LayoutInflater.from(this).inflate(R.layout.event_details_tab, null);
        tabVenue.setText("VENUE");
        tabVenue.setTextColor(this.getResources().getColorStateList(R.color.tab_text_color));
        tabVenue.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.venue, 0, 0);
        layout.getTabAt(2).setCustomView(tabVenue);

        TextView tabUpcoming = (TextView) LayoutInflater.from(this).inflate(R.layout.event_details_tab, null);
        tabUpcoming.setText("UPCOMING");
        tabUpcoming.setTextColor(this.getResources().getColorStateList(R.color.tab_text_color));
        tabUpcoming.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.upcoming, 0, 0);
        layout.getTabAt(3).setCustomView(tabUpcoming);
    }

    public class EventsDetailPagerAdaptor extends FragmentPagerAdapter {
        public EventsDetailPagerAdaptor(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                return new EventInfoFragment();
            } else if(position == 1) {
                artist = new EventArtistsFragment();
                return artist;
            } else if(position == 2) {
                return new EventVenueFragment();
            }
            return new EventUpcomingFragment();
        }

        @Override
        public int getCount() {
            return NUM_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0) {
                return "EVENT";
            } else if(position == 1) {
                return "ARTIST(S)";
            } else if(position == 2) {
                return "VENUE";
            }
            return "UPCOMING";
        }
    }
}
