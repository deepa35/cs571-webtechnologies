package com.example.deepa.entsearchapp;

public class EventDetailsData {
    private String identifier;
    private String artists;
    private String venueInfo;
    private String categoryInfo;
    private String ticketPurchaseURL;

    public EventDetailsData(String identifier, String artists, String venueInfo, String categoryInfo, String ticketPurchaseURL) {
        this.identifier = identifier;
        this.artists = artists;
        this.venueInfo = venueInfo;
        this.categoryInfo = categoryInfo;
        this.ticketPurchaseURL = ticketPurchaseURL;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getArtists() {
        return artists;
    }

    public String getVenueInfo() {
        return venueInfo;
    }

    public String getCategoryInfo() {
        return categoryInfo;
    }

    public String getTicketPurchaseURL() {
        return ticketPurchaseURL;
    }
}
