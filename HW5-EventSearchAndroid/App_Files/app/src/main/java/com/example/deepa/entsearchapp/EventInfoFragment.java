package com.example.deepa.entsearchapp;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

public class EventInfoFragment extends Fragment {
    private LinearLayout progressBarInfo;
    private TextView noInfoResults;
    private TableLayout infoTable;
    private View eventInfoView;

    private TableRow artistsRow;
    private TableRow venueRow;
    private TableRow timeRow;
    private TableRow catRow;
    private TableRow priceRow;
    private TableRow tickStatRow;
    private TableRow urlRow;
    private TableRow seatRow;

    public EventInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        eventInfoView = inflater.inflate(R.layout.fragment_event_info, container, false);
        progressBarInfo = eventInfoView.findViewById(R.id.progress_bar_info);
        noInfoResults = eventInfoView.findViewById(R.id.no_info_results);
        infoTable = eventInfoView.findViewById(R.id.infoTable);

        artistsRow = eventInfoView.findViewById(R.id.artistsRow);
        venueRow = eventInfoView.findViewById(R.id.venueRow);
        timeRow = eventInfoView.findViewById(R.id.timeRow);
        catRow = eventInfoView.findViewById(R.id.categoryRow);
        priceRow = eventInfoView.findViewById(R.id.priceRow);
        tickStatRow = eventInfoView.findViewById(R.id.ticketStatRow);
        urlRow = eventInfoView.findViewById(R.id.ticketurlRow);
        seatRow = eventInfoView.findViewById(R.id.seatMapRow);

        progressBarInfo.setVisibility(View.VISIBLE);
        noInfoResults.setVisibility(View.GONE);
        infoTable.setVisibility(View.GONE);

        artistsRow.setVisibility(View.VISIBLE);
        venueRow.setVisibility(View.VISIBLE);
        timeRow.setVisibility(View.VISIBLE);
        catRow.setVisibility(View.VISIBLE);
        priceRow.setVisibility(View.VISIBLE);
        tickStatRow.setVisibility(View.VISIBLE);
        urlRow.setVisibility(View.VISIBLE);
        seatRow.setVisibility(View.VISIBLE);

        String eventInfoReqURL = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/fetchEventDetails?eventID=";
        try {
            eventInfoReqURL = eventInfoReqURL + URLEncoder.encode(EventDetailsActivity.selectedEvent.getEventID(),"UTF-8");
        } catch(UnsupportedEncodingException error) {
            error.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, eventInfoReqURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    EventDetailsActivity.eventInfoObject = new JSONObject(response);
                    populateTable(EventDetailsActivity.eventInfoObject);

                    progressBarInfo.setVisibility(View.GONE);
                    infoTable.setVisibility(View.VISIBLE);
                } catch (JSONException error) {
                    error.printStackTrace();
                    infoTable.setVisibility(View.GONE);
                    progressBarInfo.setVisibility(View.GONE);
                    noInfoResults.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                infoTable.setVisibility(View.GONE);
                progressBarInfo.setVisibility(View.GONE);
                noInfoResults.setVisibility(View.VISIBLE);
                if(getActivity() != null) {
                    Toast.makeText(getActivity(), "Failed to get event information", Toast.LENGTH_SHORT).show();
                }
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

        return eventInfoView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if(EventDetailsActivity.eventInfoObject == null ||
                EventDetailsActivity.eventInfoObject.length() == 0 ||
                !EventDetailsActivity.selectedEvent.getEventID().equals(EventDetailsActivity.selectedEventInfo.getIdentifier())) {

        } else {
            try {
                populateTable(EventDetailsActivity.eventInfoObject);
            } catch (Exception error) {
                error.printStackTrace();
                infoTable.setVisibility(View.GONE);
                progressBarInfo.setVisibility(View.GONE);
                noInfoResults.setVisibility(View.VISIBLE);
            }
        }*/
    }

    private void populateTable(JSONObject responseObject) throws JSONException {
        String id = responseObject.get("id").toString();
        String artists = responseObject.get("artists").toString();
        String ticketURL = responseObject.get("url").toString();
        String seatMap = responseObject.get("seatMap").toString();
        String dateTime = responseObject.get("dateTime").toString();
        String venue = responseObject.get("venue").toString();
        String ticketStat = responseObject.get("ticketStat").toString();
        String category = responseObject.get("category").toString();
        String catFormatted = responseObject.get("catFormatted").toString();
        String priceRange = responseObject.get("priceRange").toString();
        EventDetailsActivity.selectedEventInfo = new EventDetailsData(id, artists, venue, category, ticketURL);

        if (artists.equals("null")) {
            artistsRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventInfoView.findViewById(R.id.artistsRowVal)).setText(artists);
            artistsRow.setVisibility(View.VISIBLE);
        }

        if (venue.equals("null")) {
            venueRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventInfoView.findViewById(R.id.venueRowVal)).setText(venue);
            venueRow.setVisibility(View.VISIBLE);
        }

        if (dateTime.equals("null")) {
            timeRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventInfoView.findViewById(R.id.timeRowVal)).setText(dateTime);
            timeRow.setVisibility(View.VISIBLE);
        }

        if (catFormatted.equals("null")) {
            catRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventInfoView.findViewById(R.id.categoryRowVal)).setText(catFormatted);
            catRow.setVisibility(View.VISIBLE);
        }

        if (priceRange.equals("null")) {
            priceRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventInfoView.findViewById(R.id.priceRowVal)).setText(priceRange);
            priceRow.setVisibility(View.VISIBLE);
        }

        if (ticketStat.equals("null")) {
            tickStatRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventInfoView.findViewById(R.id.ticketStatRowVal)).setText(ticketStat);
            tickStatRow.setVisibility(View.VISIBLE);
        }

        if (ticketURL.equals("null")) {
            urlRow.setVisibility(View.GONE);
        } else {
            TextView txt = (TextView) eventInfoView.findViewById(R.id.ticketurlRowVal);
            String textVal = "<a href='" + ticketURL + "'>Ticketmaster</a>";
            if (Build.VERSION.SDK_INT < 24) {
                txt.setText(Html.fromHtml(textVal));
            } else {
                txt.setText(Html.fromHtml(textVal, Html.FROM_HTML_MODE_COMPACT));
            }
            txt.setClickable(true);
            txt.setMovementMethod(LinkMovementMethod.getInstance());
            urlRow.setVisibility(View.VISIBLE);
        }

        if (seatMap.equals("null")) {
            seatRow.setVisibility(View.GONE);
        } else {
            TextView txt = (TextView) eventInfoView.findViewById(R.id.seatMapRowVal);
            String textVal = "<a href='" + seatMap + "'>View Here</a>";
            if (Build.VERSION.SDK_INT < 24) {
                txt.setText(Html.fromHtml(textVal));
            } else {
                txt.setText(Html.fromHtml(textVal, Html.FROM_HTML_MODE_COMPACT));
            }
            txt.setClickable(true);
            txt.setMovementMethod(LinkMovementMethod.getInstance());
            seatRow.setVisibility(View.VISIBLE);
        }
    }
}
