package com.example.deepa.entsearchapp;

import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class EventListActivity extends AppCompatActivity {
    private JSONObject requestJSON;
    private LinearLayout progressLay;
    private RecyclerView eventListRecyclerView;
    private RecyclerView.Adapter eventListAdapter;
    private RecyclerView.LayoutManager eventListLayoutManager;
    public ArrayList<EventListData> eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventlist);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressLay = findViewById(R.id.progress_bar_search);
        progressLay.setVisibility(View.VISIBLE);
        findViewById(R.id.no_search_results).setVisibility(View.GONE);
        eventList = new ArrayList<>();

        eventListRecyclerView = findViewById(R.id.search_results);
        eventListRecyclerView.setVisibility(View.GONE);
        eventListRecyclerView.setHasFixedSize(true);
        eventListRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        String requestStr = (String)getIntent().getExtras().getSerializable("requestData");
        try {
            requestJSON = new JSONObject(requestStr);
        } catch(JSONException error) {
            error.printStackTrace();
        }
        callEventListApi();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callEventListApi() {
        String eventListReqURL = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/eventSearch?keyword=";
        try {
            eventListReqURL = eventListReqURL + URLEncoder.encode(requestJSON.get("keyword").toString(),"UTF-8")+ "&category="+
                    URLEncoder.encode(requestJSON.get("category").toString(),"UTF-8")+ "&distance_unit=" + URLEncoder.encode(requestJSON.get("distance_unit").toString(),"UTF-8") + "&radioButton="+
                    URLEncoder.encode(requestJSON.get("radioButton").toString(),"UTF-8") +"&distance=" + URLEncoder.encode(requestJSON.get("distance").toString(),"UTF-8")+"&currentLocation=" +
                    URLEncoder.encode(requestJSON.get("currentLocation").toString(),"UTF-8") + "&locationInput="+ URLEncoder.encode(requestJSON.get("locationInput").toString(),"UTF-8");
        } catch(JSONException error) {
            error.printStackTrace();
        } catch(UnsupportedEncodingException error) {
            error.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, eventListReqURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray eventArray = null;
                try {
                    eventArray = new JSONArray(response);
                    for (int i = 0; i < eventArray.length(); i++) {
                        JSONObject row = eventArray.getJSONObject(i);

                        String id = row.get("id").toString();
                        String category = row.get("category").toString();
                        String name = row.get("name").toString();
                        String venue = row.get("venue").toString();
                        String dateTime = row.get("dateTime").toString();
                        String date = row.get("date").toString();
                        String time = row.get("time").toString();
                        String artists = row.get("artists").toString();

                        EventListData eventData = new EventListData(id,name,venue,dateTime,date,time,category, artists);

                        eventList.add(eventData);
                    }
                    Collections.sort(eventList, new Comparator<EventListData>() {
                        @Override
                        public int compare(EventListData o1, EventListData o2) {
                            Date date1 = null;
                            Date date2 = null;
                            Date time1 = null;
                            Date time2 = null;
                            try {
                                if(!o1.getDate().equalsIgnoreCase("null")) {
                                    date1 = new SimpleDateFormat("yyyy-MM-dd").parse(o1.getDate());
                                }
                                if(!o2.getDate().equalsIgnoreCase("null")) {
                                    date2 = new SimpleDateFormat("yyyy-MM-dd").parse(o2.getDate());
                                }
                                if(!o1.getTime().equalsIgnoreCase("null")) {
                                    time1 = new SimpleDateFormat("HH:mm:ss").parse(o1.getTime());
                                }
                                if(!o2.getTime().equalsIgnoreCase("null")) {
                                    time2 = new SimpleDateFormat("HH:mm:ss").parse(o2.getTime());
                                }
                            } catch(ParseException error) {
                                error.printStackTrace();
                            }


                            int dateSort = 0;
                            if(date1!= null && date2!=null) {
                                dateSort = date1.compareTo(date2);
                            }
                            if(dateSort != 0) {
                                return dateSort;
                            }
                            int timeSort = 0;
                            if(time1!=null && time2!=null) {
                                timeSort = time1.compareTo(time2);
                            }
                            return timeSort;
                        }

                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressLay.setVisibility(View.GONE);
                if(eventArray == null || eventArray.length() == 0) {
                    findViewById(R.id.no_search_results).setVisibility(View.VISIBLE);
                } else {
                    eventListLayoutManager = new LinearLayoutManager(EventListActivity.this);
                    eventListRecyclerView.setVisibility(View.VISIBLE);
                    eventListRecyclerView.setLayoutManager(eventListLayoutManager);
                    eventListAdapter = new EventListAdapter(eventList, EventListActivity.this);
                    eventListRecyclerView.setAdapter(eventListAdapter);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressLay.setVisibility(View.GONE);
                findViewById(R.id.no_search_results).setVisibility(View.VISIBLE);
                Toast.makeText(EventListActivity.this,"Failed to get search results",Toast.LENGTH_SHORT).show();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventListLayoutManager = new LinearLayoutManager(EventListActivity.this);
        eventListRecyclerView.setVisibility(View.VISIBLE);
        eventListRecyclerView.setLayoutManager(eventListLayoutManager);
        eventListAdapter = new EventListAdapter(eventList, EventListActivity.this);
        eventListRecyclerView.setAdapter(eventListAdapter);
    }
}
