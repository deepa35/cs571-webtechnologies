package com.example.deepa.entsearchapp;

import java.util.Objects;

public class EventListData {
    private String eventID;
    private String eventName;
    private String eventVenue;
    private String eventDateTime;
    private String eventCategory;
    private String[] eventArtists;
    private String date;
    private String time;

    public EventListData(String eventID, String eventName, String eventVenue, String eventDateTime, String date, String time, String eventCategory, String artists) {
        this.eventID = eventID;
        this.eventName = eventName;
        this.eventVenue = eventVenue;
        this.eventDateTime = eventDateTime;
        this.eventCategory = eventCategory;
        if(!artists.equals("null")) {
            String[] arr = artists.split(" \\| ");
            if(arr.length == 0) {
                this.eventArtists = null;
            } else if(arr.length == 1) {
                this.eventArtists = new String[1];
                this.eventArtists[0] = arr[0];
            } else {
                this.eventArtists = new String[2];
                for(int i=0; i<2;i++) {
                    this.eventArtists[i] = arr[i];
                }
            }
        } else {
            this.eventArtists = null;
        }
        this.date = date;
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventVenue() {
        return eventVenue;
    }

    public String getEventDateTime() {
        return eventDateTime;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public String getEventID() {
        return eventID;
    }

    public String[] getEventArtists() {
        return eventArtists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventListData that = (EventListData) o;
        return Objects.equals(eventID, that.eventID) &&
                Objects.equals(eventName, that.eventName) &&
                Objects.equals(eventVenue, that.eventVenue) &&
                Objects.equals(eventDateTime, that.eventDateTime) &&
                Objects.equals(eventCategory, that.eventCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventID, eventName, eventVenue, eventDateTime, eventCategory);
    }
}
