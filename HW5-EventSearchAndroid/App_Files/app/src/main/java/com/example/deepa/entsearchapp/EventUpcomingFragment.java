package com.example.deepa.entsearchapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class EventUpcomingFragment extends Fragment {
    View upcomingView;
    private LinearLayout progressBarUpcoming;
    private TextView noUpcoming;
    private RecyclerView upcomingList;
    private RecyclerView.Adapter upcomingListAdapter;
    private RecyclerView.LayoutManager upcomingListLayoutManager;
    private Spinner category;
    private Spinner sortOrder;
    private LinearLayout spinnerHolder;
    private ArrayList<UpcomingEventsData> upcomingDataList;
    private ArrayList<UpcomingEventsData> upcomingDataListOriginal;

    public EventUpcomingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        upcomingView = inflater.inflate(R.layout.fragment_event_upcoming, container, false);
        progressBarUpcoming = upcomingView.findViewById(R.id.progress_bar_upcoming);
        noUpcoming = upcomingView.findViewById(R.id.no_upcoming_results);
        spinnerHolder = upcomingView.findViewById(R.id.spinnerHolder);
        category = upcomingView.findViewById(R.id.upcomingCategory);
        sortOrder = upcomingView.findViewById(R.id.sortOrder);

        upcomingList = upcomingView.findViewById(R.id.upComingCardList);
        upcomingList.setHasFixedSize(true);
        upcomingListLayoutManager = new LinearLayoutManager(getActivity());
        upcomingList.setLayoutManager(upcomingListLayoutManager);
        upcomingDataList = new ArrayList<>();
        upcomingDataListOriginal = new ArrayList<>();

        progressBarUpcoming.setVisibility(View.VISIBLE);
        spinnerHolder.setVisibility(View.GONE);
        upcomingList.setVisibility(View.GONE);
        noUpcoming.setVisibility(View.GONE);

        if(!EventDetailsActivity.selectedEvent.getEventVenue().equalsIgnoreCase("null")) {
            String venueSongKickReqURL = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/getVenueIDSongkick?venue=";
            try {
                venueSongKickReqURL = venueSongKickReqURL + URLEncoder.encode(EventDetailsActivity.selectedEvent.getEventVenue(), "UTF-8");
            } catch (UnsupportedEncodingException error) {
                error.printStackTrace();
            }

            RequestQueue queue = Volley.newRequestQueue(getActivity());

            StringRequest stringRequest = new StringRequest(Request.Method.GET, venueSongKickReqURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        EventDetailsActivity.eventUpcomingObj = new JSONArray(response);
                        if(EventDetailsActivity.eventUpcomingObj != null && EventDetailsActivity.eventUpcomingObj.length() > 0) {
                            for(int i =0;i< EventDetailsActivity.eventUpcomingObj.length(); i++) {
                                JSONObject eachData = EventDetailsActivity.eventUpcomingObj.getJSONObject(i);
                                String name = eachData.get("name").toString();
                                String artist = eachData.get("artist").toString();
                                String type = eachData.get("type").toString();
                                String url = eachData.get("url").toString();
                                String date = eachData.get("date").toString();
                                String time = eachData.get("time").toString();
                                String dateTime = eachData.get("dateTime").toString();
                                UpcomingEventsData data = new UpcomingEventsData(name,artist,date,time,dateTime,url,type);
                                upcomingDataList.add(data);
                                upcomingDataListOriginal.add(data);
                            }
                            progressBarUpcoming.setVisibility(View.GONE);
                            spinnerHolder.setVisibility(View.VISIBLE);
                            upcomingList.setVisibility(View.VISIBLE);
                            upcomingListAdapter = new UpcomingEventsAdapter(upcomingDataList, getActivity());
                            upcomingList.setAdapter(upcomingListAdapter);

                        } else {
                            progressBarUpcoming.setVisibility(View.GONE);
                            spinnerHolder.setVisibility(View.VISIBLE);
                            category.setEnabled(false);
                            sortOrder.setEnabled(false);
                            noUpcoming.setVisibility(View.VISIBLE);
                            upcomingList.setVisibility(View.GONE);
                        }

                    } catch(JSONException error) {
                        error.printStackTrace();
                        progressBarUpcoming.setVisibility(View.GONE);
                        spinnerHolder.setVisibility(View.VISIBLE);
                        category.setEnabled(false);
                        sortOrder.setEnabled(false);
                        noUpcoming.setVisibility(View.VISIBLE);
                        upcomingList.setVisibility(View.GONE);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBarUpcoming.setVisibility(View.GONE);
                    spinnerHolder.setVisibility(View.VISIBLE);
                    category.setEnabled(false);
                    sortOrder.setEnabled(false);
                    noUpcoming.setVisibility(View.VISIBLE);
                    upcomingList.setVisibility(View.GONE);
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Failed to get upcoming events information", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);


            category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selection = parent.getItemAtPosition(position).toString();
                    if(selection.equalsIgnoreCase("Default")) {
                        sortOrder.setEnabled(false);
                        upcomingDataList = new ArrayList<>(upcomingDataListOriginal);
                    } else {
                        sortOrder.setEnabled(true);
                        ArrayList<UpcomingEventsData> cloneData = new ArrayList<>(upcomingDataList);
                        Collections.sort(cloneData, new sorter());
                        upcomingDataList = new ArrayList<>(cloneData);
                    }
                    upcomingListAdapter = new UpcomingEventsAdapter(upcomingDataList, getActivity());
                    upcomingList.setAdapter(upcomingListAdapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            sortOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ArrayList<UpcomingEventsData> cloneData = new ArrayList<>(upcomingDataList);
                    Collections.sort(cloneData, new sorter());
                    upcomingDataList = new ArrayList<>(cloneData);
                    upcomingListAdapter = new UpcomingEventsAdapter(upcomingDataList, getActivity());
                    upcomingList.setAdapter(upcomingListAdapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            progressBarUpcoming.setVisibility(View.GONE);
            spinnerHolder.setVisibility(View.VISIBLE);
            category.setEnabled(false);
            sortOrder.setEnabled(false);
            noUpcoming.setVisibility(View.VISIBLE);
            upcomingList.setVisibility(View.GONE);
        }
        return upcomingView;
    }

    class sorter implements Comparator<UpcomingEventsData> {
        @Override
        public int compare(UpcomingEventsData o1, UpcomingEventsData o2) {
            String sortCat = category.getSelectedItem().toString();
            String sortDir = sortOrder.getSelectedItem().toString();

            if(sortCat.equalsIgnoreCase("Event Name")) {
                if(sortDir.equalsIgnoreCase("Ascending")) {
                    return o1.getEventName().compareTo(o2.getEventName());
                }
                return o2.getEventName().compareTo(o1.getEventName());
            } else if(sortCat.equalsIgnoreCase("Artist")) {
                if(sortDir.equalsIgnoreCase("Ascending")) {
                    return o1.getArtist().compareTo(o2.getArtist());
                }
                return o2.getArtist().compareTo(o1.getArtist());
            } else if(sortCat.equalsIgnoreCase("Type")) {
                if(sortDir.equalsIgnoreCase("Ascending")) {
                    return o1.getType().compareTo(o2.getType());
                }
                return o2.getType().compareTo(o1.getType());
            } else {
                Date date1 = null;
                Date date2 = null;
                Date time1 = null;
                Date time2 = null;
                try {
                    if(!o1.getDate().equalsIgnoreCase("null")) {
                        date1 = new SimpleDateFormat("yyyy-MM-dd").parse(o1.getDate());
                    }
                    if(!o2.getDate().equalsIgnoreCase("null")) {
                        date2 = new SimpleDateFormat("yyyy-MM-dd").parse(o2.getDate());
                    }
                    if(!o1.getTime().equalsIgnoreCase("null")) {
                        time1 = new SimpleDateFormat("HH:mm:ss").parse(o1.getTime());
                    }
                    if(!o2.getTime().equalsIgnoreCase("null")) {
                        time2 = new SimpleDateFormat("HH:mm:ss").parse(o2.getTime());
                    }
                } catch(ParseException error) {
                    error.printStackTrace();
                }

                if(sortDir.equalsIgnoreCase("Ascending")) {
                    int dateSort = 0;
                    if(date1!= null && date2!=null) {
                        dateSort = date1.compareTo(date2);
                    }
                    if(dateSort != 0) {
                        return dateSort;
                    }
                    int timeSort = 0;
                    if(time1!=null && time2!=null) {
                        timeSort = time1.compareTo(time2);
                    }
                    return timeSort;
                }

                int dateSort = 0;
                if(date1!= null && date2!=null) {
                    dateSort = date2.compareTo(date1);
                }
                if(dateSort != 0) {
                    return dateSort;
                }
                int timeSort = 0;
                if(time1!=null && time2!=null) {
                    timeSort = time2.compareTo(time1);
                }
                return timeSort;
            }
        }
    }
}
