package com.example.deepa.entsearchapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

public class EventVenueFragment extends Fragment {
    private LinearLayout progressBarVenue;
    private TextView noVenueResults;
    private TableLayout venueTable;
    private View eventVenueView;

    private TableRow nameRow;
    private TableRow addressRow;
    private TableRow cityRow;
    private TableRow phoneRow;
    private TableRow openRow;
    private TableRow generalRow;
    private TableRow childRow;
    private MapViewCompatible venueMapView;
    private GoogleMap venueMap;

    public EventVenueFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        eventVenueView = inflater.inflate(R.layout.fragment_event_venue, container, false);
        progressBarVenue = eventVenueView.findViewById(R.id.progress_bar_venue);
        noVenueResults = eventVenueView.findViewById(R.id.no_venue_results);
        venueTable = eventVenueView.findViewById(R.id.venueTable);

        nameRow = eventVenueView.findViewById(R.id.venueNameRow);
        addressRow = eventVenueView.findViewById(R.id.addressRow);
        cityRow = eventVenueView.findViewById(R.id.cityRow);
        phoneRow = eventVenueView.findViewById(R.id.phoneRow);
        openRow = eventVenueView.findViewById(R.id.openHourRow);
        generalRow = eventVenueView.findViewById(R.id.generalRow);
        childRow = eventVenueView.findViewById(R.id.childRow);
        venueMapView = (MapViewCompatible) eventVenueView.findViewById(R.id.mapsView);
        venueMapView.onCreate(savedInstanceState);

        progressBarVenue.setVisibility(View.VISIBLE);
        noVenueResults.setVisibility(View.GONE);
        venueTable.setVisibility(View.GONE);

        nameRow.setVisibility(View.VISIBLE);
        addressRow.setVisibility(View.VISIBLE);
        cityRow.setVisibility(View.VISIBLE);
        phoneRow.setVisibility(View.VISIBLE);
        openRow.setVisibility(View.VISIBLE);
        generalRow.setVisibility(View.VISIBLE);
        childRow.setVisibility(View.VISIBLE);

        if(!EventDetailsActivity.selectedEvent.getEventVenue().equalsIgnoreCase("null")) {
            String eventInfoReqURL = "http://csci571androidbe.us-east-2.elasticbeanstalk.com/getVenueInfo?venue=";
            try {
                eventInfoReqURL = eventInfoReqURL + URLEncoder.encode(EventDetailsActivity.selectedEvent.getEventVenue(), "UTF-8");
            } catch (UnsupportedEncodingException error) {
                error.printStackTrace();
            }

            RequestQueue queue = Volley.newRequestQueue(getActivity());

            StringRequest stringRequest = new StringRequest(Request.Method.GET, eventInfoReqURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        EventDetailsActivity.eventVenueObject = new JSONObject(response);
                        EventDetailsActivity.venueEventIdentifier = EventDetailsActivity.selectedEvent.getEventID();
                        populateTable(EventDetailsActivity.eventVenueObject);

                        progressBarVenue.setVisibility(View.GONE);
                        venueTable.setVisibility(View.VISIBLE);
                    } catch (JSONException error) {
                        error.printStackTrace();
                        venueTable.setVisibility(View.GONE);
                        progressBarVenue.setVisibility(View.GONE);
                        noVenueResults.setVisibility(View.VISIBLE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    venueTable.setVisibility(View.GONE);
                    progressBarVenue.setVisibility(View.GONE);
                    noVenueResults.setVisibility(View.VISIBLE);
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Failed to get venue information", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);
        } else {
            venueTable.setVisibility(View.GONE);
            progressBarVenue.setVisibility(View.GONE);
            noVenueResults.setVisibility(View.VISIBLE);
        }

        return eventVenueView;
    }

    @Override
    public void onPause() {
        super.onPause();
        venueMapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if(EventDetailsActivity.eventVenueObject == null ||
                EventDetailsActivity.eventVenueObject.length() == 0 ||!EventDetailsActivity.selectedEvent.getEventID().equals(EventDetailsActivity.venueEventIdentifier)) {

        } else {
            try {
                populateTable(EventDetailsActivity.eventVenueObject);
            } catch (Exception error) {
                error.printStackTrace();
                venueTable.setVisibility(View.GONE);
                progressBarVenue.setVisibility(View.GONE);
                noVenueResults.setVisibility(View.VISIBLE);
            }
        }*/
        venueMapView.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        venueMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        venueMapView.onLowMemory();
    }

    private void populateTable(JSONObject responseObject) throws JSONException{
        String name = responseObject.get("name").toString();
        String address = responseObject.get("address").toString();
        String city = responseObject.get("city").toString();
        String phone = responseObject.get("phone").toString();
        String openHours = responseObject.get("openHours").toString();
        String general = responseObject.get("genInfo").toString();
        String child = responseObject.get("childInfo").toString();

        if(name.equals("null")) {
            nameRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventVenueView.findViewById(R.id.venueNameRowVal)).setText(name);
            nameRow.setVisibility(View.VISIBLE);
        }

        if(address.equals("null")) {
            addressRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventVenueView.findViewById(R.id.addressRowVal)).setText(address);
            addressRow.setVisibility(View.VISIBLE);
        }

        if(city.equals("null")) {
            cityRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventVenueView.findViewById(R.id.cityRowVal)).setText(city);
            cityRow.setVisibility(View.VISIBLE);
        }

        if(phone.equals("null")) {
            phoneRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventVenueView.findViewById(R.id.phoneRowVal)).setText(phone);
            phoneRow.setVisibility(View.VISIBLE);
        }

        if(openHours.equals("null")) {
            openRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventVenueView.findViewById(R.id.openHourRowVal)).setText(openHours);
            openRow.setVisibility(View.VISIBLE);
        }

        if(general.equals("null")) {
            generalRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventVenueView.findViewById(R.id.generalRowVal)).setText(general);
            generalRow.setVisibility(View.VISIBLE);
        }

        if(child.equals("null")) {
            childRow.setVisibility(View.GONE);
        } else {
            ((TextView) eventVenueView.findViewById(R.id.childRowVal)).setText(child);
            childRow.setVisibility(View.VISIBLE);
        }

        venueMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        venueMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap venue) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                venueMap = venue;

                String latitude = null;
                String longitude = null;
                try {
                    latitude = EventDetailsActivity.eventVenueObject.get("latitude").toString();
                    longitude = EventDetailsActivity.eventVenueObject.get("longitude").toString();
                } catch (Exception error) {
                    return;
                }

                if(latitude.equals("null") && longitude.equals("null")) {
                    venueMapView.setVisibility(View.GONE);
                    return;
                }
                venueMapView.setVisibility(View.VISIBLE);

                LatLng venueLatLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                venueMap.addMarker(new MarkerOptions().position(venueLatLng));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(venueLatLng).zoom(15).build();
                venueMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });


    }
}
