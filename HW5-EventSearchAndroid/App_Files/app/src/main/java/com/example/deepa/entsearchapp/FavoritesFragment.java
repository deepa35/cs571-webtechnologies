package com.example.deepa.entsearchapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import java.util.ArrayList;

public class FavoritesFragment extends Fragment {
    private FavoritesManager favManager;
    private RecyclerView favListRecyclerView;
    private RecyclerView.Adapter favListAdapter;
    private RecyclerView.LayoutManager favListLayoutManager;
    public static TextView noFavView;

    public FavoritesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View favView = inflater.inflate(R.layout.fragment_favorites, container, false);
        favManager = new FavoritesManager();
        noFavView = favView.findViewById(R.id.no_fav_results);
        ArrayList<EventListData> favEventList = favManager.retrieveFavorites(MainActivity.getContext());
        favListRecyclerView =  favView.findViewById(R.id.fav_results);
        favListRecyclerView.setHasFixedSize(true);
        favListRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        if(favEventList == null || favEventList.size() <= 0) {
            noFavView.setVisibility(View.VISIBLE);
        } else {
            noFavView.setVisibility(View.GONE);
            favListLayoutManager = new LinearLayoutManager(getActivity());
            favListRecyclerView.setLayoutManager(favListLayoutManager);
            favListAdapter = new FavoritesListAdapter(favEventList, getActivity());
            favListRecyclerView.setAdapter(favListAdapter);
        }

        return favView;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
          try {
              InputMethodManager keyboardManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
              keyboardManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
              keyboardManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
          } catch (Exception error) {
              error.printStackTrace();
          }
        }
    }

    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
        ArrayList<EventListData> favEventList = favManager.retrieveFavorites(MainActivity.getContext());
        favListLayoutManager = new LinearLayoutManager(getActivity());
        favListRecyclerView.setLayoutManager(favListLayoutManager);
        favListAdapter = new FavoritesListAdapter(favEventList, getActivity());
        ((FavoritesListAdapter) favListAdapter).notifyDataSetChangedCustom();
        favListRecyclerView.setAdapter(favListAdapter);
        if(favEventList == null || favEventList.size() <= 0) {
            noFavView.setVisibility(View.VISIBLE);
        } else {
            noFavView.setVisibility(View.GONE);
        }
    }
}
