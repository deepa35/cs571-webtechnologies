package com.example.deepa.entsearchapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import java.util.ArrayList;

public class FavoritesListAdapter extends RecyclerView.Adapter<EventListAdapter.EventListViewHolder> {
    private ArrayList<EventListData> eventListDataset;
    private LayoutInflater viewInflater;
    private Context context;
    private FavoritesManager  favManager;

    public FavoritesListAdapter(ArrayList<EventListData> elDataset, Context context) {
        eventListDataset = elDataset;
        viewInflater = LayoutInflater.from(context);
        this.context = context;
        favManager = new FavoritesManager();
    }

    public static class EventListViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout eachRow;
        String eventID;
        ImageView catIcon;
        TextView eventName;
        TextView eventVenue;
        TextView eventDate;
        ImageView eventFavIcon;

        EventListViewHolder(View eachEvent) {
            super(eachEvent);
            eachRow = eachEvent.findViewById(R.id.eachEvent);
            eventID = "";
            catIcon = eachEvent.findViewById(R.id.category_icon);
            eventName = eachEvent.findViewById(R.id.line1);
            eventVenue = eachEvent.findViewById(R.id.line2);
            eventDate = eachEvent.findViewById(R.id.line3);
            eventFavIcon = eachEvent.findViewById(R.id.fav_icon);
        }
    }

    @Override
    public EventListAdapter.EventListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = viewInflater.inflate(R.layout.activity_each_event, parent, false);
        EventListAdapter.EventListViewHolder viewHolder = new EventListAdapter.EventListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final EventListAdapter.EventListViewHolder holder, final int position) {
        EventListData eachEventData = eventListDataset.get(position);

        String id = eachEventData.getEventID();
        String category = eachEventData.getEventCategory();
        int catImageId = -1;
        String name = eachEventData.getEventName();
        String venue = eachEventData.getEventVenue();
        String dateTime = eachEventData.getEventDateTime();
        if(category.equals("N/A")) {
            holder.catIcon.setImageResource(android.R.color.transparent);
        } else {
            if (category.equalsIgnoreCase("Music")) {
                catImageId = R.drawable.music_icon;
            } else if (category.equalsIgnoreCase("Sports")) {
                catImageId = R.drawable.sport_icon;
            } else if (category.equalsIgnoreCase("Film")) {
                catImageId = R.drawable.film_icon;
            } else if (category.equalsIgnoreCase("Miscellaneous")) {
                catImageId = R.drawable.miscellaneous_icon;
            } else if (category.equalsIgnoreCase("Arts & Theatre")) {
                catImageId = R.drawable.art_icon;
            }
            holder.catIcon.setImageResource(catImageId);
        }

        holder.eventName.setText(name);
        holder.eventVenue.setText(venue);
        holder.eventDate.setText(dateTime);
        holder.eventID = id;
        holder.eventFavIcon.setImageResource(R.drawable.heart_fill_red);

        holder.eventFavIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toastMessage = eventListDataset.get(position).getEventName() + " was removed from favorites";
                favManager.removeFromFavorite(MainActivity.getContext(), eventListDataset.get(position));
                eventListDataset.remove(position);
                notifyDataSetChangedCustom();
                Toast.makeText(context,toastMessage,Toast.LENGTH_SHORT).show();
            }
        });

        holder.eachRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventListData selectedEvent = eventListDataset.get(position);
                String selectedEventJSON = new Gson().toJson(selectedEvent);
                Intent eventIntent = new Intent(context,EventDetailsActivity.class);
                eventIntent.putExtra("eventData", selectedEventJSON);
                context.startActivity(eventIntent);
            }
        });
    }

    public void notifyDataSetChangedCustom() {
        if(eventListDataset == null ||getItemCount() == 0) {
            FavoritesFragment.noFavView.setVisibility(View.VISIBLE);
        } else {
            FavoritesFragment.noFavView.setVisibility(View.GONE);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(eventListDataset == null) {
            return 0;
        }
        return eventListDataset.size();
    }
}
