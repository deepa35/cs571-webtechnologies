package com.example.deepa.entsearchapp;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.google.gson.Gson;

public class FavoritesManager {
    private String SHARED_PREF_CLASS = "EVENT_SEARCH_APP_FAVLIST";
    private String FAVORITES = "fav_events_key";

    public FavoritesManager() {
        super();
    }

    public void addToFavorite(Context context, EventListData event) {
        ArrayList<EventListData> favEvents = retrieveFavorites(context);
        if (favEvents == null) {
            favEvents = new ArrayList<EventListData>();
        }
        favEvents.add(event);
        SharedPreferences favPref = context.getSharedPreferences(SHARED_PREF_CLASS, Context.MODE_PRIVATE);
        SharedPreferences.Editor favEditor = favPref.edit();
        String jsonFavorites = new Gson().toJson(favEvents);
        favEditor.putString(FAVORITES, jsonFavorites);
        favEditor.commit();
    }

    public void removeFromFavorite(Context context, EventListData event) {
        ArrayList<EventListData> favEvents = retrieveFavorites(context);
        if (favEvents != null && favEvents.size()>0) {
            favEvents.remove(event);
            SharedPreferences favPref = context.getSharedPreferences(SHARED_PREF_CLASS, Context.MODE_PRIVATE);
            SharedPreferences.Editor favEditor = favPref.edit();
            String jsonFavorites = new Gson().toJson(favEvents);
            favEditor.putString(FAVORITES, jsonFavorites);
            favEditor.commit();
        }
    }

    public ArrayList<EventListData> retrieveFavorites(Context context) {
        SharedPreferences favPref = context.getSharedPreferences(SHARED_PREF_CLASS, Context.MODE_PRIVATE);
        List<EventListData> favorites;

        if (favPref.contains(FAVORITES)) {
            String jsonFavorites = favPref.getString(FAVORITES, null);
            EventListData[] favoriteItems = new Gson().fromJson(jsonFavorites, EventListData[].class);
            favorites = Arrays.asList(favoriteItems);
            return new ArrayList<EventListData>(favorites);
        }
        return null;
    }

    public boolean isInFavorites(EventListData event, Context context) {
        List<EventListData> favorites = retrieveFavorites(context);
        if (favorites != null) {
            for (EventListData eachEvent : favorites) {
                if (eachEvent.equals(event)) {
                   return true;
                }
            }
        }
        return false;
    }
}
