package com.example.deepa.entsearchapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private static int NUM_TABS = 2;
    private SearchFavPagerAdaptor mSearchFavPagerAdaptor;
    private ViewPager mViewPager;
    private LocationManager lm;
    private Location location;
    public static double currentLat = 0;
    public static double currentLong = 0;
    public static final int LOCATION_REQUESTS = 1;
    private static Context mainContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewPager = (ViewPager) findViewById(R.id.searchfav_view_pager);
        mSearchFavPagerAdaptor = new SearchFavPagerAdaptor(getSupportFragmentManager());
        mViewPager.setAdapter(mSearchFavPagerAdaptor);
        mainContext = this;
        TabLayout layout = (TabLayout) findViewById(R.id.searchfav_tablayout);
        layout.setupWithViewPager(mViewPager);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            // Permission denied
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUESTS);
        } else {
            //Permission received
            lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location == null) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        currentLat = location.getLatitude();
                        currentLong = location.getLongitude();
                        SearchFragment.enableSearchButton();
                        //lm.removeUpdates(this);
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                });
                /*currentLat = 34.0266;
                currentLong = -118.283;*/
            } else {
                currentLat = location.getLatitude();
                currentLong = location.getLongitude();
            }
        }
    }

    public static Context getContext() {
        return mainContext;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUESTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {
                        //Permission received
                        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if(location == null) {
                            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    currentLat = location.getLatitude();
                                    currentLong = location.getLongitude();
                                    //lm.removeUpdates(this);
                                }

                                @Override
                                public void onStatusChanged(String provider, int status, Bundle extras) {

                                }

                                @Override
                                public void onProviderEnabled(String provider) {

                                }

                                @Override
                                public void onProviderDisabled(String provider) {

                                }
                            });
                            /*currentLat = 34.0266;
                            currentLong = -118.283;*/
                        } else {
                            currentLat = location.getLatitude();
                            currentLong = location.getLongitude();
                        }
                        SearchFragment.enableSearchButton();

                    }
                } else {
                    SearchFragment.disableSearchButton();
                }
                return;
            }
        }

    }

    public class SearchFavPagerAdaptor extends FragmentPagerAdapter {
        public SearchFavPagerAdaptor(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                return new SearchFragment();
            }
            return new FavoritesFragment();
        }

        @Override
        public int getCount() {
            return NUM_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0) {
                return "SEARCH";
            }
            return "FAVORITES";
        }
    }

}
