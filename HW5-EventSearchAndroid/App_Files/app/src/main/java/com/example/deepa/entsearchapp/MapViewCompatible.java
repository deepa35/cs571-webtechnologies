package com.example.deepa.entsearchapp;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;

public class MapViewCompatible extends MapView {

    public MapViewCompatible(Context context) {
        super(context);
    }

    public MapViewCompatible(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MapViewCompatible(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MapViewCompatible(Context context, GoogleMapOptions options) {
        super(context, options);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.dispatchTouchEvent(ev);
    }
}
