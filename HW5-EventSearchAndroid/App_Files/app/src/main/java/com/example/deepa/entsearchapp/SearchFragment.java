package com.example.deepa.entsearchapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {
    private AppCompatAutoCompleteTextView searchKeywordElem; //To be changed
    private Spinner categoryElem;
    private EditText distanceElem;
    private Spinner disUnitElem;
    private RadioGroup locRadioGroup;
    private EditText otherLocElem;

    private TextView keywordErrorElem;
    private TextView locationErrorElem;

    private static Button searchButton;
    private static Button clearButton;

    //Auto Complete Helpers
    private static final int TRIGGER_AUTO_COMPLETE = 100;
    private static final long AUTO_COMPLETE_DELAY = 1000;
    private Handler handler;
    private AutoSuggestAdapter autoSuggestAdapter;

    public SearchFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View searchView = inflater.inflate(R.layout.fragment_search, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        searchKeywordElem = (AppCompatAutoCompleteTextView) searchView.findViewById(R.id.keywordTextBox);

        categoryElem = (Spinner)searchView.findViewById(R.id.categoryDropDown);
        distanceElem = (EditText) searchView.findViewById(R.id.distance);
        disUnitElem = (Spinner) searchView.findViewById(R.id.distanceUnitDropDown);
        locRadioGroup = (RadioGroup) searchView.findViewById(R.id.locButtonGroup);
        otherLocElem = (EditText) searchView.findViewById(R.id.locationTextBox);
        searchButton = (Button) searchView.findViewById(R.id.searchButton);
        clearButton = (Button) searchView.findViewById(R.id.clearButton);
        keywordErrorElem = (TextView) searchView.findViewById(R.id.keyword_error_msg);
        locationErrorElem = (TextView) searchView.findViewById(R.id.otherLoc_error_msg);

        if(MainActivity.currentLat == 0 || MainActivity.currentLong == 0) {
            searchButton.setEnabled(false);
        }

        //Setting up the adapter for AutoSuggest
        autoSuggestAdapter = new AutoSuggestAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line);
        searchKeywordElem.setThreshold(2);
        searchKeywordElem.setAdapter(autoSuggestAdapter);

        searchKeywordElem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                keywordErrorElem.setVisibility(View.GONE);
                handler.removeMessages(TRIGGER_AUTO_COMPLETE);
                handler.sendEmptyMessageDelayed(TRIGGER_AUTO_COMPLETE, AUTO_COMPLETE_DELAY);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                if (msg.what == TRIGGER_AUTO_COMPLETE) {
                    if (!TextUtils.isEmpty(searchKeywordElem.getText())) {
                        autoCompleteApiCall(searchKeywordElem.getText().toString());
                    }
                }
                return false;
            }
        });

        otherLocElem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                locationErrorElem.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        locRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup locGroup, int selectedLoc){
                switch(selectedLoc) {
                    case R.id.hereButton:
                        otherLocElem.setText("");
                        otherLocElem.setEnabled(false);
                        break;
                    case R.id.otherLocButton:
                        otherLocElem.setEnabled(true);
                        break;
                }
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean keyWordSuccess = false;
                boolean otherLocSuccess;

                String keyword = searchKeywordElem.getText().toString();
                String otherLocation = otherLocElem.getText().toString();
                String category = categoryElem.getSelectedItem().toString();
                String distance = distanceElem.getText().toString();
                String distanceUnit = disUnitElem.getSelectedItem().toString();

                int selectedRadioID = locRadioGroup.getCheckedRadioButtonId();
                String radioIdentifier = ((RadioButton)locRadioGroup.findViewById(selectedRadioID)).getText().toString();

                if((keyword.length() == 0 || keyword.trim().length()== 0)) {
                    keyWordSuccess = false;
                } else {
                    keyWordSuccess = true;
                }

                if(selectedRadioID == R.id.hereButton) {
                    otherLocSuccess = true;
                } else {
                    if((otherLocation.length() == 0 || otherLocation.trim().length() == 0)) {
                        otherLocSuccess = false;
                    } else {
                        otherLocSuccess = true;
                    }
                }

                if(!keyWordSuccess) {
                    keywordErrorElem.setVisibility(View.VISIBLE);
                }
                if(!otherLocSuccess) {
                    locationErrorElem.setVisibility(View.VISIBLE);
                }
                if(!keyWordSuccess || !otherLocSuccess) {
                    Toast.makeText(getActivity(),"Please fix all fields with errors",Toast.LENGTH_SHORT).show();
                } else {
                    //Submit
                    //Category
                    String catInJSONReq = "";
                    if (category.equals("All")) {
                        catInJSONReq = "default";
                    } else if(category.equals("Music")) {
                        catInJSONReq = "music";
                    } else if(category.equals("Sports")) {
                        catInJSONReq = "sports";
                    } else if(category.equals("Arts & Theatre")) {
                        catInJSONReq = "arts";
                    } else if(category.equals("Film")) {
                        catInJSONReq = "film";
                    } else if(category.equals("Miscellaneous")) {
                        catInJSONReq = "miscellaneous";
                    }

                    //Distance unit
                    String disUnitInJSONReq = "";
                    if(distanceUnit.equals("Miles")) {
                        disUnitInJSONReq = "miles";
                    } else if(distanceUnit.equals("Kilometers")) {
                        disUnitInJSONReq = "km";
                    }

                    //RadioSelection
                    String radioOptInJSONReq = "";
                    if(radioIdentifier.equals("Current Location")) {
                        radioOptInJSONReq = "here";
                    } else if (radioIdentifier.equals("Other. Specify location")){
                        radioOptInJSONReq = "others";
                    }

                    JSONObject currentLocJSON = new JSONObject();
                    try {
                        currentLocJSON.put("lat", MainActivity.currentLat);
                        currentLocJSON.put("lon", MainActivity.currentLong);
                    } catch(JSONException error) {
                        error.printStackTrace();
                    }

                    JSONObject requestJSON = new JSONObject();
                    try {
                        requestJSON.put("keyword",keyword);
                        requestJSON.put("category", catInJSONReq);
                        requestJSON.put("distance_unit", disUnitInJSONReq);
                        requestJSON.put("radioButton", radioOptInJSONReq);
                        requestJSON.put("distance", distance);
                        requestJSON.put("locationInput", otherLocation);
                        requestJSON.put("currentLocation", currentLocJSON);
                    } catch(JSONException error) {
                        error.printStackTrace();
                    }

                    Intent eventListIntent = new Intent(getActivity(),EventListActivity.class);
                    eventListIntent.putExtra("requestData",requestJSON.toString());
                    startActivity(eventListIntent);
                }
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                searchKeywordElem.setText("");
                distanceElem.setText("");
                categoryElem.setSelection(0);
                disUnitElem.setSelection(0);
                otherLocElem.setText("");
                locRadioGroup.check(R.id.hereButton);
                keywordErrorElem.setVisibility(View.GONE);
                locationErrorElem.setVisibility(View.GONE);
            }
        });

        return searchView;
    }

    private void autoCompleteApiCall(String text) {
        AutoCompleteAPI.make(getActivity(), text, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                List<String> stringList = new ArrayList<>();
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject row = array.getJSONObject(i);
                        stringList.add(row.getString("name"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                autoSuggestAdapter.setData(stringList);
                autoSuggestAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        });
    }

    public static void disableSearchButton() {
        searchButton.setEnabled(false);
    }

    public static void enableSearchButton() {
        searchButton.setEnabled(true);
    }
}
