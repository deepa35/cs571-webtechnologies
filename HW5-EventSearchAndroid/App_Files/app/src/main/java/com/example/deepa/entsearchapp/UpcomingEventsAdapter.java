package com.example.deepa.entsearchapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class UpcomingEventsAdapter extends RecyclerView.Adapter<UpcomingEventsAdapter.UpcomingEventsViewHolder> {
    private ArrayList<UpcomingEventsData> upcomingEventsList;
    private LayoutInflater viewInflater;
    private Context context;

    public UpcomingEventsAdapter(ArrayList<UpcomingEventsData> upcomingEventsList, Context context) {
        this.upcomingEventsList = upcomingEventsList;
        viewInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public static class UpcomingEventsViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView eventName;
        TextView artistName;
        TextView dateTime;
        TextView eventType;

        UpcomingEventsViewHolder(View eachUp) {
            super(eachUp);
            card = eachUp.findViewById(R.id.card);
            eventName = eachUp.findViewById(R.id.upEventName);
            artistName = eachUp.findViewById(R.id.upArtist);
            dateTime = eachUp.findViewById(R.id.upDateTime);
            eventType = eachUp.findViewById(R.id.upType);
        }

    }

    @Override
    public UpcomingEventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = viewInflater.inflate(R.layout.upcoming_cards, parent, false);
        UpcomingEventsViewHolder upcomingHolder = new UpcomingEventsViewHolder(view);
        return upcomingHolder;
    }

    @Override
    public void onBindViewHolder(final UpcomingEventsViewHolder holder, final int position) {
        UpcomingEventsData eachData = upcomingEventsList.get(position);

        String name = eachData.getEventName();
        String artist = eachData.getArtist();
        String date = eachData.getDateTime();
        String type = eachData.getType();
        final String url = eachData.getUrl();

        if(!name.equalsIgnoreCase("null")) {
            holder.eventName.setText(name);
        } else {
            holder.eventName.setVisibility(View.GONE);
        }

        if(!artist.equalsIgnoreCase("null")) {
            holder.artistName.setText(artist);
        } else {
            holder.artistName.setVisibility(View.GONE);
        }

        if(!date.equalsIgnoreCase("null")) {
            holder.dateTime.setText(date);
        } else {
            holder.dateTime.setVisibility(View.GONE);
        }

        if(!type.equalsIgnoreCase("null")) {
            holder.eventType.setText("Type: "+type);
        } else {
            holder.eventType.setVisibility(View.GONE);
        }

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!url.equalsIgnoreCase("null")) {
                    Intent songKick = new Intent(Intent.ACTION_VIEW);
                    songKick.setData(Uri.parse(url));
                    context.startActivity(songKick);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if(upcomingEventsList == null) {
            return 0;
        }
        return upcomingEventsList.size();
    }
}
