package com.example.deepa.entsearchapp;

import java.util.Objects;

public class UpcomingEventsData {
    private String eventName;
    private String artist;
    private String date;
    private String time;
    private String dateTime;
    private String url;
    private String type;

    public UpcomingEventsData(String eventName, String artist, String date, String time, String dateTime, String url, String type) {
        this.eventName = eventName;
        this.artist = artist;
        this.date = date;
        this.time = time;
        this.dateTime = dateTime;
        this.url = url;
        this.type = type;
    }

    public String getEventName() {
        return eventName;
    }

    public String getArtist() {
        return artist;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpcomingEventsData that = (UpcomingEventsData) o;
        return Objects.equals(eventName, that.eventName) &&
                Objects.equals(artist, that.artist) &&
                Objects.equals(date, that.date) &&
                Objects.equals(time, that.time) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(url, that.url) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventName, artist, date, time, dateTime, url, type);
    }
}
