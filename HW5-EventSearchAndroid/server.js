const express = require('express');
const geohash = require('ngeohash');
const path = require('path')
const bodyParser = require('body-parser');
const request = require('request');
const promiseReq = require('request-promise');
const SpotifyWebAPI = require('spotify-web-api-node');
const moment = require('moment');

const TICKETMASTER_API_KEY = "Buwj0G2A846oGezBw3N05bgYt9YgUx5s";
const GOOGLE_API_KEY = "AIzaSyDSvNSOBQhqi-J0jNCHQeYT60p9ES4OLig";
const SPOTIFY_CLIENT_ID = "daa4c39dc189407084bdbba3e64a7517";
const SPOTIFY_SECRET = "2131616c78ac4664b2394351673bd262";
const GOOGLE_CUSTOM_SEARCH_KEY = "AIzaSyAd7AWR3GZaxrkSMCS7k09aTQsFFIn6PIo";
const GOOGLE_SEARCH_ENGINE_ID = "011142998701674357804:u8bdjz-s64k";
const SONGKICK_API_KEY = "QoIMKkvzOECUmf1W";

const spotifyApi = new SpotifyWebAPI({
    clientId: SPOTIFY_CLIENT_ID,
    clientSecret: SPOTIFY_SECRET
});

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "app")));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get("/",function(req,res){
    res.send("Mobile backend for CSCI571 Homework 9");
});

app.get('/getVenueIDSongkick', async function(req, res) {
    var venueName = req.query.venue;
    var songKickVenueIDURL = "https://api.songkick.com/api/3.0/search/venues.json?query="+ encodeURIComponent(venueName)+"&apikey="+SONGKICK_API_KEY;
    request(songKickVenueIDURL, function (error, response, body) {
        if (error) {
            res.sendStatus(503);
        } else {
            if (response.statusCode == 200) {
                var venueJSON = JSON.parse(body);
                var id;
                try {id = venueJSON.resultsPage.results.venue[0].id; } catch(error) {}
                let upcomingEventURL = "https://api.songkick.com/api/3.0/venues/"+id+"/calendar.json?apikey="+SONGKICK_API_KEY;
                request(upcomingEventURL, function (errorUp, responseUp, bodyUp) {
                    if (errorUp) {
                        res.sendStatus(503);
                    } else {
                        if (responseUp.statusCode == 200) {
                            let upcomingEventJSON = JSON.parse(bodyUp);
                            let crudeData = []
                            let output = []
                            try {
                                crudeData = upcomingEventJSON.resultsPage.results.event;
                                var max = crudeData.length < 5 ? crudeData.length : 5;
                                for(var i = 0; i<max; i++) {
                                    let eachData = crudeData[i];
                                    let eachDataJSON = {};

                                    //Name
                                    if(eachData.displayName) {
                                        eachDataJSON.name = eachData.displayName;
                                    } else {
                                        eachDataJSON.name = "null";
                                    }

                                    //Artist
                                    if(eachData.performance && eachData.performance.length > 0 &&
                                        eachData.performance[0].artist && eachData.performance[0].artist.displayName) {
                                        eachDataJSON.artist = eachData.performance[0].artist.displayName;
                                    } else {
                                        eachDataJSON.artist = "null";
                                    }

                                    //Type
                                    if(eachData.type) {
                                        eachDataJSON.type = eachData.type;
                                    } else {
                                        eachDataJSON.type = "null";
                                    }

                                    //URL
                                    if(eachData.uri) {
                                        eachDataJSON.url = eachData.uri;
                                    } else {
                                        eachDataJSON.url = "null";
                                    }

                                    //Date
                                    let date = "";
                                    if(eachData.start && eachData.start.date) {
                                        date = eachData.start.date;
                                    } else {
                                        date = "null";
                                    }
                                    eachDataJSON.date = date;

                                    //Time
                                    let time = "";
                                    if(eachData.start && eachData.start.time) {
                                        time = eachData.start.time;
                                    } else {
                                        time = "null";
                                    }
                                    eachDataJSON.time = time;

                                    //date time
                                    if(date!= "null" && time!= "null") {
                                        eachDataJSON.dateTime = moment(date).format('MMM DD, YYYY') + " " + time;
                                    } else if(date!="null" && time == "null") {
                                        eachDataJSON.dateTime = moment(date).format('MMM DD, YYYY');
                                    } else if(date=="null" && time!="null") {
                                        eachDataJSON.dateTime = time;
                                    } else {
                                        eachDataJSON.dateTime = "null";
                                    }
                                    output.push(eachDataJSON);
                                }
                                res.json(output);
                            } catch(error) {
                                res.json([])
                            }
                        } else {
                            res.sendStatus(responseUp.statusCode);
                        }
                    }
                });
            } else {
                res.sendStatus(response.statusCode);
            }
        }
    });
});

app.get('/getArtistInfoSpotify', async function(req, res) {
    var artistName = req.query.artists;
    var result;

    if (!artistName || artistName == "Undefined") {
        res.json({});
    } else {
        spotifyApi.searchArtists(artistName).then(function(data) {
            try {
                var out = data.body.artists.items;
                if(out) {
                    var artistData;
                    for(var i =0; i< out.length; i++) {
                        if(out[i].name && (out[i].name).toLowerCase() == artistName.toLowerCase()) {
                            artistData = out[i];
                            break;
                        }
                    }
                    if(!artistData) {
                        result = {}
                    } else {
                        result = {};
                        //name
                        if(artistData.name) {
                            result.name = artistData.name;
                        } else {
                            result.name = "null";
                        }

                        //followers
                        if(artistData.followers && artistData.followers.total) {
                            result.follow = (artistData.followers.total).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        } else {
                            result.follow = "null";
                        }

                        //popularity
                        if(artistData.popularity) {
                            result.popular = artistData.popularity;
                        } else {
                            result.popular = "null";
                        }

                        //link
                        if(artistData.external_urls && artistData.external_urls.spotify) {
                            result.url = artistData.external_urls.spotify;
                        } else {
                            result.url = "null";
                        }
                    }
                } else {
                    result = {};
                }
                res.json(result);
            } catch(error) {
                res.json({});
            }
        },function(error) {
            if(error.statusCode == 401) {
                spotifyApi.clientCredentialsGrant().then(
                    function(data) {
                        spotifyApi.setAccessToken(data.body['access_token']);
                        spotifyApi.searchArtists(artistName).then(function(data) {
                            try {
                                var out = data.body.artists.items;
                                if(out) {
                                    var artistData;
                                    for(var i =0; i< out.length; i++) {
                                        if(out[i].name && (out[i].name).toLowerCase() == artistName.toLowerCase()) {
                                            artistData = out[i];
                                            break;
                                        }
                                    }
                                    if(!artistData) {
                                        result = {};
                                    } else {
                                        result = {};
                                        //name
                                        if(artistData.name) {
                                            result.name = artistData.name;
                                        } else {
                                            result.name = "null";
                                        }

                                        //followers
                                        if(artistData.followers && artistData.followers.total) {
                                            result.follow = (artistData.followers.total).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else {
                                            result.follow = "null";
                                        }

                                        //popularity
                                        if(artistData.popularity) {
                                            result.popular = artistData.popularity;
                                        } else {
                                            result.popular = "null";
                                        }

                                        //link
                                        if(artistData.external_urls && artistData.external_urls.spotify) {
                                            result.url = artistData.external_urls.spotify;
                                        } else {
                                            result.url = "null";
                                        }
                                    }
                                } else {
                                    result = {};
                                }
                                 res.json(result);
                            } catch(error) {
                                res.json({});
                            }

                        }, function (er) {
                            res.json({});
                        });
                    },
                    function(err) {
                        res.json({});
                    }
                );
            }else {
                res.json({});
            }
        });
    }

});

app.get('/customSearchImage',async function(req,res) {
    var artistArray = req.query.artist;
    if(!Array.isArray(artistArray)) {
        let str = artistArray;
        artistArray = [];
        artistArray.push(str)
    }
    var result = [];
    for(var i=0; i< artistArray.length; i++) {
        var name = artistArray[i];
        if(!name || name == "Undefined") {
            result.push("null");
        } else {
            var artistImageFetchURL = "https://www.googleapis.com/customsearch/v1?q=" + encodeURIComponent(name) + "&cx="+GOOGLE_SEARCH_ENGINE_ID+"&imgSize=large&imgType=news&num=8&searchType=image&key="+GOOGLE_CUSTOM_SEARCH_KEY;
            try {
                var fetchResult = await promiseReq(artistImageFetchURL);
                var artistData = JSON.parse(fetchResult);
                result.push(artistData)
            } catch(error) {
                result.push("null");
            }
        }
    }
    let output = {};

    for(var i=0; i<result.length; i++) {
        let indexnum = i + 1;
        if(result[i] != "null") {
            let items = result[i].items;
            if (!items || items.length == 0) {
                output['artist' + indexnum] = ["null"]
            } else {
                let images = [];
                for (var j = 0; j < items.length; j++) {
                    if (items[j].link) {
                        images.push(items[j].link);
                    } else {
                        images.push("null");
                    }
                }
                output['artist' + indexnum] = images;
            }
        } else {
            output['artist' + indexnum] = ["null"];
        }
    }
    res.json(output);

});

app.get('/getVenueInfo', async function(req,res) {
    var venueName = req.query.venue;
    var venueFetchURL = "https://app.ticketmaster.com/discovery/v2/venues.json?apikey=" + TICKETMASTER_API_KEY +"&keyword="+ encodeURIComponent(venueName);
    var output = {};
    setTimeout(function() {
        request(venueFetchURL, function (error, response, body) {
            if (error) {
                res.sendStatus(503);
            } else {
                if (response.statusCode == 200) {
                    var venueJSON = JSON.parse(body);
                    var venueData;
                    try {
                        venueData = venueJSON._embedded.venues[0];

                        //name
                        if(venueData.name) {
                            output.name = venueData.name;
                        } else {
                            output.name = "null";
                        }

                        //address
                        if(venueData.address && venueData.address.line1) {
                            output.address = venueData.address.line1;
                        } else {
                            output.address = "null";
                        }

                        //city
                        if(venueData.city && venueData.city.name && venueData.state && venueData.state.name) {
                            output.city = venueData.city.name + ", " + venueData.state.name;
                        } else if(venueData.city && venueData.city.name && (!venueData.state || !venueData.state.name)) {
                            output.city = venueData.city.name;
                        } else if(venueData.state && venueData.state.name && (!venueData.city || !venueData.city.name)) {
                            output.city = venueData.state.name;
                        } else {
                            output.city = "null";
                        }

                        //phone
                        if(venueData.boxOfficeInfo && venueData.boxOfficeInfo.phoneNumberDetail) {
                            output.phone = venueData.boxOfficeInfo.phoneNumberDetail;
                        } else {
                            output.phone = "null";
                        }

                        //openhours
                        if(venueData.boxOfficeInfo && venueData.boxOfficeInfo.openHoursDetail) {
                            output.openHours = venueData.boxOfficeInfo.openHoursDetail;
                        } else {
                            output.openHours = "null";
                        }

                        //generalInfo
                        if(venueData.generalInfo && venueData.generalInfo.generalRule) {
                            output.genInfo = venueData.generalInfo.generalRule;
                        } else {
                            output.genInfo = "null";
                        }

                        //childInfo
                        if(venueData.generalInfo && venueData.generalInfo.childRule) {
                            output.childInfo = venueData.generalInfo.childRule;
                        } else {
                            output.childInfo = "null";
                        }

                        //latitude
                        if(venueData.location && venueData.location.latitude) {
                            output.latitude = venueData.location.latitude;
                        } else {
                            output.latitude = "null";
                        }

                        //longitude
                        if(venueData.location && venueData.location.longitude) {
                            output.longitude = venueData.location.longitude;
                        } else {
                            output.longitude = "null";
                        }
                        res.json(output);
                    } catch(error) {
                        res.json({});
                    }
                } else {
                    res.sendStatus(response.statusCode);
                }
            }
        });
    }, 1000);
});

app.get('/fetchEventDetails',async function(req, res) {
    var eventID = req.query.eventID;
    var detailsReqUrl = "https://app.ticketmaster.com/discovery/v2/events/"+ eventID + ".json?apikey="+TICKETMASTER_API_KEY;
    var output = {};
    setTimeout(function() { request(detailsReqUrl,function(error,response,body) {
        if (error) {
            res.sendStatus(503);
        } else {
            if (response.statusCode == 200) {
                var detailsJSON = JSON.parse(body);
                output.id = detailsJSON.id;

                //ticket url
                if(detailsJSON.url) {
                    output.url = detailsJSON.url;
                } else {
                    output.url = "null"
                }

                //seat map
                if(detailsJSON.seatmap && detailsJSON.seatmap.staticUrl) {
                    output.seatMap = detailsJSON.seatmap.staticUrl;
                } else {
                    output.seatMap = "null";
                }

                //date time
                if(detailsJSON.dates.start && detailsJSON.dates.start.localDate && detailsJSON.dates.start.localTime) {
                    output.dateTime = moment(detailsJSON.dates.start.localDate).format('MMM DD, YYYY') + " " + detailsJSON.dates.start.localTime;
                } else if(detailsJSON.dates.start && detailsJSON.dates.start.localDate && !detailsJSON.dates.start.localTime) {
                    output.dateTime = moment(detailsJSON.dates.start.localDate).format('MMM DD, YYYY');
                } else if(detailsJSON.dates.start && !detailsJSON.dates.start.localDate && detailsJSON.dates.start.localTime) {
                    output.dateTime = detailsJSON.dates.start.localTime;
                } else {
                    output.dateTime = "null";
                }

                //venue
                if(detailsJSON._embedded && detailsJSON._embedded.venues && detailsJSON._embedded.venues.length > 0 && detailsJSON._embedded.venues[0].name) {
                    output.venue = detailsJSON._embedded.venues[0].name;
                } else {
                    output.venue = "null";
                }

                //ticket status
                if(detailsJSON.dates && detailsJSON.dates.status && detailsJSON.dates.status.code) {
                    output.ticketStat = detailsJSON.dates.status.code.charAt(0).toUpperCase() + detailsJSON.dates.status.code.slice(1);
                } else {
                    output.ticketStat = "null";
                }

                //category
                if(detailsJSON.classifications && detailsJSON.classifications.length > 0 && detailsJSON.classifications[0].segment && detailsJSON.classifications[0].segment.name) {
                    output.category = detailsJSON.classifications[0].segment.name;
                } else {
                    output.category = "null";
                }

                //category formatted
                if(detailsJSON.classifications && detailsJSON.classifications.length > 0 && detailsJSON.classifications[0].genre && detailsJSON.classifications[0].genre.name) {
                    if(output.category != "null") {
                        output.catFormatted = output.category + " | " + detailsJSON.classifications[0].genre.name;
                    } else {
                        output.catFormatted = detailsJSON.classifications[0].genre.name;
                    }
                } else {
                    if(output.category != "null") {
                        output.catFormatted = output.category;
                    } else {
                        output.catFormatted = "null";
                    }
                }

                //price range
                if(detailsJSON.priceRanges && detailsJSON.priceRanges.length>0 && detailsJSON.priceRanges[0].min && detailsJSON.priceRanges[0].max) {
                    output.priceRange = "$" + parseFloat(detailsJSON.priceRanges[0].min) + " ~ " + "$" + parseFloat(detailsJSON.priceRanges[0].max);
                } else if(detailsJSON.priceRanges && detailsJSON.priceRanges.length>0 && detailsJSON.priceRanges[0].min && !detailsJSON.priceRanges[0].max) {
                    output.priceRange = "$" + parseFloat(detailsJSON.priceRanges[0].min);
                } else if(detailsJSON.priceRanges && detailsJSON.priceRanges.length>0 && !detailsJSON.priceRanges[0].min && detailsJSON.priceRanges[0].max) {
                    output.priceRange = "$" + parseFloat(detailsJSON.priceRanges[0].max);
                } else {
                    output.priceRange = "null";
                }

                //artists
                if(detailsJSON._embedded && detailsJSON._embedded.attractions && detailsJSON._embedded.attractions.length > 0) {
                    output.artists = detailsJSON._embedded.attractions.map(function (each) {
                            return each.name
                    }).join(" | ");
                } else {
                    output.artists = "null";
                }

                res.json(output);
            } else {
                res.sendStatus(response.statusCode);
            }
        }
    });
    }, 700);
});

app.get('/eventSearch', async function(req,res) {
    var keyword = req.query.keyword;
    var category = req.query.category;
    var distanceUnit = req.query.distance_unit;
    var radioButton = req.query.radioButton;
    var distance = req.query.distance;
    var location = null;
    try { location = JSON.parse(req.query.currentLocation); } catch(error) {console.log("current location JSON parse failed")}
    var locationName = req.query.locationInput;
    var sourceLat = null;
    var sourceLong = null;
    var hash = "";
    var segmentID = "";
    let resultArray = [];

    if(!distance) {
        distance = 10;
    }

    if(radioButton == "here") {
        sourceLat = location.lat;
        sourceLong = location.lon;
        hash = geohash.encode(sourceLat,sourceLong);
    } else {
        var googleMapReqUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURIComponent(locationName) + "&key=" + GOOGLE_API_KEY;
        try{
            let response = await promiseReq(googleMapReqUrl);
            let locationJSON = JSON.parse(response);
            sourceLat = parseFloat(locationJSON.results[0].geometry.location.lat);
            sourceLong = parseFloat(locationJSON.results[0].geometry.location.lng);
            hash = geohash.encode(sourceLat,sourceLong);
        } catch(error){}
    }
    if (category == "default") {
        segmentID = "";
    } else if (category == "music") {
        segmentID = "KZFzniwnSyZfZ7v7nJ";
    } else if (category == "sports") {
        segmentID = "KZFzniwnSyZfZ7v7nE";
    } else if (category == "arts") {
        segmentID = "KZFzniwnSyZfZ7v7na";
    } else if (category == "film") {
        segmentID = "KZFzniwnSyZfZ7v7nn";
    } else {
        segmentID = "KZFzniwnSyZfZ7v7n1";
    }

    let ticketMasterURL = "https://app.ticketmaster.com/discovery/v2/events.json?apikey=" + TICKETMASTER_API_KEY + "&keyword=" + encodeURIComponent(keyword) + "&segmentId=" + segmentID + "&radius=" + distance + "&unit="+ distanceUnit +"&geoPoint="+ hash;
    setTimeout(function() {request(ticketMasterURL,function(error,response,body) {
        if (error) {
            res.sendStatus(503);
        } else {
            if (response.statusCode == 200) {
                var eventDetailsJSON = JSON.parse(body);
                var output = "";
                try {
                    output=eventDetailsJSON._embedded.events;
                    for(var i=0; i<output.length; i++) {
                        var eventJSON = {};
                        if(output[i].id) {
                            eventJSON.id = output[i].id;
                        }
                        if(output[i].name) {
                            eventJSON.name = output[i].name;
                        } else {
                            eventJSON.name = "Sorry, event name unavailable";
                        }
                        if(output[i]._embedded &&  output[i]._embedded.venues &&
                            output[i]._embedded.venues.length>0 && output[i]._embedded.venues[0].name) {
                            eventJSON.venue = output[i]._embedded.venues[0].name;
                        } else {
                            eventJSON.venue = "Sorry, venue details unavailable";
                        }
                        let dateTime = "N/A";
                        if(output[i].dates && output[i].dates.start) {
                            if(output[i].dates.start.localDate && output[i].dates.start.localTime) {
                                dateTime = output[i].dates.start.localDate + " " + output[i].dates.start.localTime;
                            } else if(output[i].dates.start.localDate && !output[i].dates.start.localTime) {
                                dateTime = output[i].dates.start.localDate;
                            } else if(!output[i].dates.start.localDate && output[i].dates.start.localTime) {
                                dateTime = output[i].dates.start.localTime;
                            }
                        }
                        eventJSON.dateTime = dateTime;

                        if(output[i].dates && output[i].dates.start && output[i].dates.start.localDate) {
                            eventJSON.date = output[i].dates.start.localDate;
                        } else {
                            eventJSON.date = "null";
                        }

                        if(output[i].dates && output[i].dates.start && output[i].dates.start.localTime) {
                            eventJSON.time = output[i].dates.start.localTime;
                        } else {
                            eventJSON.time = "null";
                        }

                        if(output[i]._embedded &&  output[i]._embedded.attractions &&
                            output[i]._embedded.attractions.length>0) {
                            eventJSON.artists = output[i]._embedded.attractions.map(function (each) {
                                return each.name
                            }).join(" | ");
                        } else {
                            eventJSON.artists = "null";
                        }


                        let category = "N/A";
                        if(output[i].classifications && output[i].classifications.length>0 && output[i].classifications[0].segment && output[i].classifications[0].segment.name) {
                            category = output[i].classifications[0].segment.name;
                        }
                        eventJSON.category = category;
                        resultArray.push(eventJSON);
                    }
                    res.json(resultArray);
                } catch(error) {
                    res.json([])
                }

            } else {
                res.sendStatus(response.statusCode);
            }
        }
    })}, 700);

});

app.get('/autoComplete',async function(req, res) {
    var requestData = req.query.keyword;
    var auto_url = "https://app.ticketmaster.com/discovery/v2/suggest?apikey="+TICKETMASTER_API_KEY+"&keyword="+encodeURIComponent(requestData);

    request(auto_url,function(error,response,body){
        if(error){
            res.sendStatus(503);
        }else{
            if(response.statusCode == 200){
                var suggestionJSON = JSON.parse(body);
                var output;
                try {
                    output = suggestionJSON._embedded.attractions;
                    res.json(output);
                }catch(error){
                    res.json([]);
                }
            } else {
                res.sendStatus(response.statusCode);
            }
        }
    })
});

app.listen(process.env.PORT || 8000, function(){
    console.log("ESA Server Listening");
});
